﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.DAL;
using MyOB.Interface;
using ErrorLog;

namespace MyOB.BAL
{
    public class SuppliersClass
    {
        SuppliersDAL _objSuppliers;
        /// <summary>
        /// Suppliers class constructor
        /// </summary>
        /// <param name="conn">Pass MyOB connection setting,for this is use MYOConnectionSetting class object</param>
        public SuppliersClass(MYOConnectionSetting conn)
        {
            try
            {
                _objSuppliers = new SuppliersDAL(conn);
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in SuppliersClass class, function name SuppliersClass[MyOB.BAL] :-" + ex.Message);
                throw;
            }
        }
        /// <summary>
        /// Add new supplier
        /// </summary>
        /// <param name="_supplier">Pass SuppliersList class object</param>
        public void AddSupplier(SuppliersList supplier)
        {
            try
            {
                _objSuppliers.AddSupplier(supplier);
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in SuppliersClass class, function name AddSupplier [MyOB.BAL]:-" + ex.Message);
                throw;
            }
        }
        /// <summary>
        /// checkSupplierIsExists fucntion for checking supplier is exist or not 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool checkSupplierIsExists(string name)
        {
            try
            {
                //select * from Cards
                return _objSuppliers.checkSupplierIsExists(name);
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in SuppliersClass class, function name checkSupplierIsExists [MyOB.BAL]:-" + ex.Message);
                throw;
            }
        }
    }
}
