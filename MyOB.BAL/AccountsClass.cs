﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.DAL;
using MyOB.Interface;
using System.Data;

namespace MyOB.BAL
{ 
   public class AccountsClass
   {
       AccountsDAL _objAccounts;
        public AccountsClass(MYOConnectionSetting conn)
          {
              _objAccounts = new AccountsDAL(conn);
          }

        public DataTable getPurchasesAccounts()
        {
            try
            {
                return _objAccounts.getPurchasesAccounts();

            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public DataTable getSaleAccounts()
        {
            try
            {
                return _objAccounts.getSaleAccounts();

            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public DataTable getTradeDebtorsAccounts()
        {
            try
            {
                return _objAccounts.getTradeDebtorsAccounts();

            }
            catch (Exception ex)
            {

                return null;
            }
        }
    }
}
