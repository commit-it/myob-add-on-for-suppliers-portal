﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.DAL;
using MyOB.Interface;
using System.Data;
using System.Data.Odbc;

namespace MyOB.BAL
{
   public class PurchasesClass
    {
       PurchasesDAL _objPurchases;
       public PurchasesClass(MYOConnectionSetting conn)
       {
           _objPurchases = new PurchasesDAL(conn); 
       }
       public DataSet GetPurchases()
       {
           try
           {

               return _objPurchases.GetPurchases();

           }
           catch (Exception ex)
           {


               throw;
           }
       }
       public DataSet MySQlSelect()
       {
           try
           {

               return _objPurchases.MySQlSelect();

           }
           catch (Exception ex)
           {


               throw;
           }
       }
    }
}
