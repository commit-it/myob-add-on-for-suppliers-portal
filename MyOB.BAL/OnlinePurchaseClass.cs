﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.DAL;
using MyOB.Interface;

namespace MyOB.BAL
{

    public class OnlinePurchaseClass
    {
         OnlinePurchaseDAL onlinePurchase;
         /// <summary>
         /// Online purchases' class constructor
         /// </summary>
         /// <param name="conn">Pass myob connection settings</param>
         /// <param name="MySQlConn">Pass Online mysql connection string</param>
         public OnlinePurchaseClass(MYOConnectionSetting conn, String MySQlConn)
         {
             try
             {
                 onlinePurchase = new OnlinePurchaseDAL(conn, MySQlConn);
             }
             catch (Exception)
             {
                 
                 throw;
             }
         }

         public void importPurchaseOrder()
         {
             try
             {
                 onlinePurchase.importPurchaseOrder();
             }
             catch (Exception)
             {
                 
                 throw;
             }
         }

    }
}
