﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.DAL;
using MyOB.Interface;
using System.Data;
namespace MyOB.BAL
{
    public class SaleClass
    {

        SaleDAL _objSale;
        List<SaleList> _list = new List<SaleList>();
        /// <summary>
        /// SaleClass class constructor
        /// </summary>
        /// <param name="conn">Pass MYOB connection setting</param>
        /// <param name="MySQlConn">Pass mysql connection setting</param>
        public SaleClass(MYOConnectionSetting conn, String MySQlConn)
        {
            _objSale = new SaleDAL(conn,MySQlConn);
        }
        /// <summary>
        /// Get data from sale table
        /// </summary>
        /// <returns>SaleList object</returns>
        public List<SaleList> getSale()
        {
            try
            {

                return _objSale.GetSale();
            }
            catch (Exception)
            {

                 throw;
            }
        }

      

    }
   
}
