﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.DAL;
using MyOB.Interface;
using ErrorLog;

namespace MyOB.BAL
{
   public class MySQLDataSyncClass
   {
       MySQLDataSyncDAL _objMySQLDataSync;
       /// <summary>
       /// MySQLDataSyncDAL class constructor
       /// </summary>
       /// <param name="conn">Pass MYOB connection setting</param>
       /// <param name="MySQlConn">Pass mysql connection setting</param>
       public MySQLDataSyncClass(MYOConnectionSetting conn, String MySQlConn,string GLNNumber)
       {
           _objMySQLDataSync = new MySQLDataSyncDAL(conn, MySQlConn,GLNNumber);
       }
       /// <summary>
       /// Import Invoice from mysql database 
       /// </summary>
       public string importInvoice(myobDataImportValues _myobDataImportValues,out string _message)
       {
           try
           {
               return _objMySQLDataSync.importInvoice(_myobDataImportValues, out _message);
           }
           catch (Exception ex)
           {
               ErrorClass.WriteError("Error in SuppliersClass class, function name checkSupplierIsExists [MyOB.BAL]:-" + ex.Message);
               throw;
           }
       }
       public string importUpdatedInvoice(myobDataImportValues _myobDataImportValues, out string _message)
       {
           try
           {
               return _objMySQLDataSync.importUpdatedInvoice(_myobDataImportValues, out _message);
           }
           catch (Exception ex)
           {
               ErrorClass.WriteError("Error in SuppliersClass class, function name checkSupplierIsExists [MyOB.BAL]:-" + ex.Message);
               throw;
           }
       }
    }
}
