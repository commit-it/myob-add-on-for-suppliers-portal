﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.DAL;
using MyOB.Interface;
using ErrorLog;

namespace MyOB.BAL
{
   public class CustomersClass
    {
          CustomersDAL _objCustomers;
          List<CustomersList> _list = new List<CustomersList>();
          /// <summary>
          /// Customers class constructor
          /// </summary>
          /// <param name="DataBaseFilePath">Database file path</param>
          public CustomersClass(MYOConnectionSetting conn, String MySQlConn)
          {
              _objCustomers = new CustomersDAL(conn,MySQlConn);
          }
          /// <summary>
          /// Get data from Customers table
          /// </summary>
          /// <returns>Customers List object</returns>
          public List<CustomersList> getCustomers()
          {
              try
              {

                  return _objCustomers.getCustomers();
              }
              catch (Exception ex)
              {
                  ErrorClass.WriteError("Error in ItemsClass class, function name checkItemIsExists[MyOB.BAL] :-" + ex.Message);
                  throw;
              }
          }
    }
}
