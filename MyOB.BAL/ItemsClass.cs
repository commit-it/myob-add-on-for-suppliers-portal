﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.Interface;
using MyOB.DAL;
using ErrorLog;

namespace MyOB.BAL
{
  public  class ItemsClass
    {
          ItemsDAL _objItems;
          List<ItemsList> _list = new List<ItemsList>();
          /// <summary>  
          /// Items class constructor
          /// </summary>
          /// <param name="DataBaseFilePath">Database file path</param>
          public ItemsClass(MYOConnectionSetting conn)
          {
              _objItems = new ItemsDAL(conn);
          }
          /// <summary>
          /// Get data from Items table
          /// </summary>
          /// <returns>ItemsList object</returns>
          public List<ItemsList> getItems()
          {
              try
              {

                  return _objItems.GetItem();
              }
              catch (Exception ex)
              {
                  ErrorClass.WriteError("Error in ItemsClass class, function name getItems [MyOB.BAL]:-" + ex.Message);
                  throw;
              }
          }
          /// <summary>
          /// checkItemIsExists function for checking item is exists in myob database or not.
          /// </summary>
          /// <param name="name">Item Name</param>
          public void checkItemIsExists(string name)
          {
              try
              {
                  _objItems.checkItemIsExists(name);
              }
              catch (Exception ex)
              {
                  ErrorClass.WriteError("Error in ItemsClass class, function name checkItemIsExists [MyOB.BAL]:-" + ex.Message);
                  throw;
              }
          }
    }
}
