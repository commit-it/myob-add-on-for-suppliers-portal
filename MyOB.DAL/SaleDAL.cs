﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using MyOB.Interface;
using System.Data;
using MySql.Data.MySqlClient;
namespace MyOB.DAL
{
 

   public class SaleDAL
    {
       List<SaleList> _listSale = new List<SaleList>();
       ConnectionCLS _connectionCLS = new ConnectionCLS();
       OdbcCommand _command = new OdbcCommand();
       MYOConnectionSetting _conn;
       String _MySQlConn;
       string InvoiceId = string.Empty;
       /// <summary>
       /// SaleDAL class constructor
       /// </summary>
       /// <param name="conn">Pass MYOB connection setting</param>
       /// <param name="MySQlConn">Pass mysql connection setting</param>
           public SaleDAL(MYOConnectionSetting conn, String MySQlConn)
       {
           _conn = conn;
           _MySQlConn = MySQlConn;
       }
       /// <summary>
       /// Get data from sale table
       /// </summary>
       /// <returns>List<SaleList> object</returns>
       public List<SaleList> GetSale()
       {
           try
           {
               OdbcCommand _command = new OdbcCommand("Select * from Sales");

           _connectionCLS.ConnectionOpen(_conn);
           _connectionCLS.MySQlConnectionOpen(_MySQlConn);
           _command.Connection = _connectionCLS.odbc;

           OdbcDataReader _dataReader = _command.ExecuteReader();
           int fCount = _dataReader.FieldCount;
           while (_dataReader.Read())
           {
               _listSale.Add(new SaleList
               {
                   SaleID =Convert.ToInt32( _dataReader["SaleID"]),
                   InvoiceNumber = _dataReader["InvoiceNumber"].ToString(),
                   CustomerPONumber = _dataReader["CustomerPONumber"].ToString(),
                   IsHistorical = _dataReader["CustomerPONumber"].ToString(),
                   BackorderSaleID = _dataReader.GetInt32(5),
                   InvoiceDate =Convert.ToDateTime(_dataReader["InvoiceDate"]),
                   InvoiceStatusID = _dataReader["CustomerPONumber"].ToString(),

                   TotalLines = Convert.ToDouble(_dataReader["TotalLines"]),
                   TotalTax=Convert.ToDouble(_dataReader["TotalTax"]),
                   TotalPaid = Convert.ToDouble(_dataReader["TotalPaid"]),
                   TotalDeposits = Convert.ToDouble(_dataReader["TotalDeposits"]),
                   TotalCredits = Convert.ToDouble(_dataReader["TotalCredits"]),
                   TotalDiscount = Convert.ToDouble(_dataReader["TotalDiscounts"]),
                   OutStandingBalance = Convert.ToDouble(_dataReader["OutStandingBalance"]),
                   StatusID = _dataReader["InvoiceStatusID"].ToString(),
                   //ShipToAddress = _dataReader["ShipToAddress"].ToString(),
                  // Memo = _dataReader["Memo"].ToString()
                   
               });
               
           }

            //close and dispose DataReader object
           _dataReader.Close();
           _dataReader.Dispose();
           _connectionCLS.odbc.Close();
           return _listSale;
           }
           catch (Exception)
           {
               
               throw;
           }
       }
   

    }
}
