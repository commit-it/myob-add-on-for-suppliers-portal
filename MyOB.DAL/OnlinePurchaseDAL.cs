﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.Interface;
using System.Data;
using MySql.Data.MySqlClient;


namespace MyOB.DAL
{
  public class OnlinePurchaseDAL
  {
      ConnectionCLS _connectionCLS = new ConnectionCLS();
      MYOConnectionSetting _conn;
      String _MySQlConn;
      /// <summary>
      /// Online purchases' class constructor
      /// </summary>
      /// <param name="conn">Pass myob connection settings</param>
      /// <param name="MySQlConn">Pass Online mysql connection string</param>
      public OnlinePurchaseDAL(MYOConnectionSetting conn, String MySQlConn)
      {
          try
          {
           
              _conn = conn;
              _MySQlConn = MySQlConn;
          }
          catch (Exception)
          {
              
              throw;
          }
      }

      /// <summary>
      /// import orders from online database
      /// </summary>
      public void importPurchaseOrder()
      {
          string _query = string.Empty;
            _query+=" select PRC.purchase_order_number,";
            _query+=" PRC.seller_gln,";
            _query+=" PRC.bill_to_gln,";
            _query+=" PRC.bill_to_name,";
            _query+=" PRC.bill_to_address,";
            _query+=" PRC.order_date,";
            _query+=" PRC.deliver_date,";
            _query+=" PRC.total_no_of_items,";
            _query+=" PRC.total_order_amount,";
            _query+=" CUR.currency_code,";
            _query+=" PRC.comments,";
            _query+=" PRC.purchase_order_status_id,";
            _query+=" POS.status as purchase_order_statuses,";
            _query+=" STA.status,";
            _query+=" PRC.modified,";
            _query+=" PRC.created,";
            _query+=" INV.sub_total,";
            _query+=" INV.tax,";
            _query+=" INV.sales_person,";
            _query+=" INV.ship_via, ";
            _query+=" POI.item_number, ";
            _query+=" POI.description, ";
            _query+=" POI.net_amount, ";
            _query+=" POI.net_price, ";
            _query+=" POI.requested_quantity, ";
            _query+=" POI.pack_quantity  ";

            _query+=" from purchase_orders as PRC";
            _query+=" inner join";
            _query+=" invoices as INV on PRC.purchase_order_number=INV.purchase_order_number";
            _query+=" inner join";
            _query+=" currencies as CUR on CUR.id=PRC.currency_id";
            _query+=" inner join";
            _query+=" purchase_order_statuses as POS on POS.id=PRC.purchase_order_status_id";
            _query+=" inner join ";
            _query+=" status as STA on STA.id=PRC.status_id";
            _query+=" inner join ";
            _query+=" purchase_order_items as POI on POI.purchase_order_id=PRC.id ";


          try
          {
            
                  _connectionCLS.ConnectionOpen(_conn);
                  _connectionCLS.MySQlConnectionOpen(_MySQlConn);
                  MySqlDataAdapter dataAdapter = new MySqlDataAdapter(_query, _connectionCLS.mySqlConn);
                  DataSet ds = new DataSet();
                  dataAdapter.Fill(ds);

                  if (ds != null)
                  {
                      //SuppliersClass _objSuppliers = new SuppliersClass(_conn);
                      for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                      {
                         

                          PurchasesItemList _objPurchasesItem = new PurchasesItemList
                          {
                              CoLastName = ds.Tables[0].Rows[i]["sales_person"].ToString(),
                              FirstName = ds.Tables[0].Rows[i]["bill_to_name"].ToString(),
                              AddressLine1 = ds.Tables[0].Rows[i]["bill_to_address"].ToString(),
                              PurchaseNumber = ds.Tables[0].Rows[i]["purchase_order_number"].ToString(),
                              PurchaseDate = ds.Tables[0].Rows[i]["order_date"].ToString(),
                              ShipVia = ds.Tables[0].Rows[i]["ship_via"].ToString(),
                              DeliveryStatus = "P",

                              ItemNumber = ""
                          };

                         
                          _query = string.Empty;
                        
                      }
                   
                  }
              
             
          }
          catch (Exception)
          {
              
              throw;
          }
      }


    }
}
