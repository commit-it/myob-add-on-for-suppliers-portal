﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ErrorLog;
using System.Globalization;

namespace MyOB.DAL
{
   public static class Common
    {
      /// <summary>
        /// MyOB file alow only system date format, therefore with the help of this function set your date format
      /// </summary>
      /// <param name="date">date string</param>
        /// <returns>return date in format</returns>
       public static string setDateformat(string date)
       {
           try
           {  //
               string sysFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
               if (sysFormat == "")
               {
                   sysFormat = "MM-dd-yy";
               }
               return Convert.ToDateTime(date).ToString(sysFormat);
           }
           catch (Exception ex)
           {
               ErrorClass.WriteError("Error in Common class, function name setDateformat[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
       /// <summary>
       /// mysqlDateformat function working for set date format for mysql database
       /// </summary>
       /// <param name="date">date string</param>
       /// <returns>return date into dd-MM-yy HH:mm:ss format</returns>
       public static string mysqlDateformat(string date)
       {
           try
           {  //

               return Convert.ToDateTime(date).ToString("yy-MM-dd HH:mm:ss");
           }
           catch (Exception ex)
           {
               ErrorClass.WriteError("Error in Common class, function name setDateformat[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
    }
}
