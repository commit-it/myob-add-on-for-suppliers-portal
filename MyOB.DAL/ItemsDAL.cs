﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.Interface;
using System.Data.Odbc;
using System.Data;
using ErrorLog;

namespace MyOB.DAL
{
 
   public class ItemsDAL
    {
       ConnectionCLS _connectionCLS = new ConnectionCLS();
       List<ItemsList> _list = new List<ItemsList>();
       OdbcCommand _command;
       string _query = string.Empty;
       MYOConnectionSetting _conn;
       /// <summary>
       /// Items class constructor
       /// </summary>
       /// <param name="MYOConnectionSetting">Pass MYOConnectionSetting</param>
       public ItemsDAL(MYOConnectionSetting conn)
       {
           _conn = conn;
       }
       /// <summary>
       /// Get data from Items table
       /// </summary>
       /// <returns>List<ItemsList> object</returns>
       public List<ItemsList> GetItem()
       {
           try
           {
               OdbcCommand _command = new OdbcCommand("Select * from Items");
               //Open myob file connection 
               _connectionCLS.ConnectionOpen(_conn);
               _command.Connection = _connectionCLS.odbc;

               OdbcDataReader _dataReader = _command.ExecuteReader();
               int fCount = _dataReader.FieldCount;
               while (_dataReader.Read())
               {
                   _list.Add(new ItemsList
                   {
                       ItemID = Convert.ToInt32(_dataReader["ItemID"]),
                       ItemName = _dataReader["ItemName"].ToString(),
                       ItemNumber = _dataReader["ItemNumber"].ToString(),
                       QuantityOnHand = Convert.ToDouble(_dataReader["QuantityOnHand"]),
                       ValueOnHand = Convert.ToDouble(_dataReader["ValueOnHand"]),
                       PositiveAverageCost = Convert.ToDouble(_dataReader["PositiveAverageCost"]),
                       LastUnitPrice = Convert.ToDouble(_dataReader["LastUnitPrice"]),
                       SellOnOrder = Convert.ToDouble(_dataReader["SellOnOrder"]),
                       BaseSellingPrice = Convert.ToDouble(_dataReader["BaseSellingPrice"]),
                       TaxInclusiveLastPurchasePrice = Convert.ToDouble(_dataReader["TaxInclusiveLastPurchasePrice"]),
                       Description = _dataReader["ItemDescription"].ToString()
                   });

               }
               //close and dispose DataReader object
               _dataReader.Close();
               _dataReader.Dispose();
               _connectionCLS.odbc.Close();
               return _list;
           }
           catch (Exception ex) 
           {
               ErrorClass.WriteError("Error in ItemsDAL class, function name GetItem[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
       /// <summary>
       /// checkItemIsExists function for checking item is exists in myob database or not.
       /// </summary>
       /// <param name="name">Item Name</param>
       public string  checkItemIsExists(string name)
       {
           try
           {
                _command = new OdbcCommand("Select Item.* from Items Item where (Item.ItemName='" + name + "')");
               //Open myob file connection 
               _connectionCLS.ConnectionOpen(_conn);
               _command.Connection = _connectionCLS.odbc;
               OdbcDataReader _dataReader = _command.ExecuteReader();
               int fCount = _dataReader.FieldCount;
               while (_dataReader.Read())
               {
                   _list.Add(new ItemsList
                   {
                       ItemNumber = _dataReader["ItemNumber"].ToString()
                   });

               }
               //close and dispose DataReader object
               _dataReader.Close();
               _dataReader.Dispose();
               _connectionCLS.odbc.Close();
               /// get max item id
                GetMaxItemID();
              
               if (_list.Count != 0)
               {
                   return _list[0].ItemNumber;
               }
               else
               {
                   return "0";
               }

           }
           catch (Exception ex)
           {
               ErrorClass.WriteError("Error in ItemsDAL class, function name checkItemIsExists[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
       /// <summary>
       /// Get Max itemid from invertroy table
       /// </summary>
       public Int32 GetMaxItemID()
       {
           try
           {
               Int32 _itemId = 0;
               //InventoryAdjustmentLines
               using (OdbcCommand _command = new OdbcCommand("Select ItemID from  Items Inventory order by Inventory.ItemID desc"))
               {
                   //Open myob file connection 
                   _connectionCLS.ConnectionOpen(_conn);
                   _command.Connection = _connectionCLS.odbc;
                   _itemId = Convert.ToInt32(_command.ExecuteScalar());
               }
               _connectionCLS.ConnectionClose();
               return _itemId;
               //close and dispose DataReader object
             
           }
           catch (Exception ex)
           {
               _connectionCLS.ConnectionClose();
               ErrorClass.WriteError("Error in ItemsDAL class, function name GetMaxItemID[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
       //InventoryAdjustmentLines
       /// <summary>
       /// With the help of "Import_Items" table add new item in items table 
       /// </summary>
       /// <param name="item">pass ItemsList class object </param>
       public void AddItem(ItemsList item)
       {
           try
           {//Import_Items
               _connectionCLS.ConnectionOpen(_conn);
               OdbcCommand _command = new OdbcCommand();
  
               _query = string.Empty;
               _query += "(ItemNumber,ItemName,Description,UseDescriptionOnSale,SellingPrice,StandardCost,NumberItemsBuyUnit,InactiveItem,Inventory,Buy,Sell,AssetAccount,IncomeAccount,ExpenseAccount,CustomField1,TaxCodeWhenBought,TaxCodeWhenSold,SellUnitMeasure,BuyUnitMeasure) VALUES ";
               _query += "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
               //_query += "('" + item.ItemNumber + "','" + item.ItemName + "','" + item.Description + "','" + item.Description + "'," + item.SellingPrice + "," + item.StandardCost + "," + item.Quantity + ",'N','Y','Y','Y'," + item.AssetAccount + "," + item.IncomeAccount + "," + item.ExpenseAccount + ",'" + item.Barcode + "','" + item.TaxCodeWhenBought + "','" + item.TaxCodeWhenBought + "','" + item.SellUnitMeasure + "','" + item.SellUnitMeasure + "')";
               _command.CommandType = CommandType.Text; 
               _command.CommandText = "Insert into Import_Items " + _query;
               _command.Parameters.AddWithValue("@ItemNumber", item.ItemNumber);
               _command.Parameters.AddWithValue("@ItemName", item.ItemName);
               _command.Parameters.AddWithValue("@Description", item.Description);
               _command.Parameters.AddWithValue("@UseDescriptionOnSale", item.Description);
               _command.Parameters.AddWithValue("@SellingPrice", item.SellingPrice);
               _command.Parameters.AddWithValue("@StandardCost", item.StandardCost);
               _command.Parameters.AddWithValue("@NumberItemsBuyUnit", item.Quantity);
               _command.Parameters.AddWithValue("@InactiveItem", "N");
               _command.Parameters.AddWithValue("@Inventory", "Y");
               _command.Parameters.AddWithValue("@Buy", "Y");
               _command.Parameters.AddWithValue("@Sell", "Y");
               _command.Parameters.AddWithValue("@AssetAccount", item.AssetAccount);
               _command.Parameters.AddWithValue("@IncomeAccount", item.IncomeAccount);
               _command.Parameters.AddWithValue("@ExpenseAccount", item.ExpenseAccount);
               _command.Parameters.AddWithValue("@CustomField1", item.Barcode);
               _command.Parameters.AddWithValue("@TaxCodeWhenBought",item.TaxCodeWhenBought);
               _command.Parameters.AddWithValue("@TaxCodeWhenSold", item.TaxCodeWhenBought);
               _command.Parameters.AddWithValue("@SellUnitMeasure", item.SellUnitMeasure);
               _command.Parameters.AddWithValue("@BuyUnitMeasure", item.SellUnitMeasure);
               _command.Connection = _connectionCLS.odbc;
               Int32 _status=   _command.ExecuteNonQuery();
               _command.CommandText = "END TRANSACTION";
               _command.ExecuteNonQuery();

               //###############---With the help "Import_Inventory_Adjustments" table, Add all Items in inventory table
               _query = string.Empty;
               _query += "(AdjustmentDate,Memo,ItemNumber,Quantity,UnitCost,Amount,Account) VALUES ";
               _query += "('" + Common.setDateformat(DateTime.UtcNow.ToString()) + "','" + item.Memo + "','" + item.ItemNumber + "'," + item.Quantity + "," + item.StandardCost + "," + item.SellingPrice + ",11200)";
               _command.CommandText = " Insert into  Import_Inventory_Adjustments " + _query;

               _command.ExecuteNonQuery();
               _command.CommandText = "END TRANSACTION";
               _command.ExecuteNonQuery();


               _command.Connection.Close();
               _command.Connection.Dispose();

           }
           catch (Exception ex)
           {
               _command.Connection.Close();
               _command.Connection.Dispose();
               ErrorClass.WriteError("Error in ItemsDAL class, function name AddItem[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="barcodeNumber">Pass barcodeNumber for geting item number from Items table</param>
       /// <returns>Item Number</returns>
       public string GetItemNumber(String barcodeNumber)
       {
           string _itemNumber = string.Empty;
           try
           {
               //Open myob file connection 
               _connectionCLS.ConnectionOpen(_conn);
               using (OdbcCommand _command = new OdbcCommand("Select ItemNumber from Items where CustomField1='" + barcodeNumber + "'"))
               {
                   _command.Connection = _connectionCLS.odbc;
                    _itemNumber=   Convert.ToString(_command.ExecuteScalar());

                   if (_itemNumber == string.Empty)
                   {
                       using (OdbcCommand _command1 = new OdbcCommand("Select ItemNumber from Items where CustomField2='" + barcodeNumber + "'"))
                       {
                           _command1.Connection = _connectionCLS.odbc;
                           _itemNumber = Convert.ToString(_command1.ExecuteScalar());
                       }
                       _connectionCLS.ConnectionClose();
                   }

                   if (_itemNumber == string.Empty)
                   {

                       _connectionCLS.ConnectionOpen(_conn);
                       using (OdbcCommand _command2 = new OdbcCommand("Select ItemNumber from Items where CustomField3='" + barcodeNumber + "'"))
                       {
                           _command2.Connection = _connectionCLS.odbc;
                           _itemNumber = Convert.ToString(_command2.ExecuteScalar());
                        
                       }

                   }
                   //close and dispose DataReader object
                   _connectionCLS.ConnectionClose();

                   return _itemNumber;
               }
           }
           catch (Exception ex)
           {
               _connectionCLS.ConnectionClose();
               ErrorClass.WriteError("Error in ItemsDAL class, function name AddItem[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }

    }
}
