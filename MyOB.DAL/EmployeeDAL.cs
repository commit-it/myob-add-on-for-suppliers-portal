﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using MyOB.Interface;
using System.Data;
using ErrorLog;

namespace MyOB.DAL
{

  public class EmployeeDAL
    {
        OdbcCommand _command;
        ConnectionCLS _connectionCLS = new ConnectionCLS();
        MYOConnectionSetting _conn;
        /// <summary>
        /// EmployeeDAL class constructor
        /// </summary>
        /// <param name="conn">Pass MyOB connection setting,for this is use MYOConnectionSetting class object</param>
        public EmployeeDAL(MYOConnectionSetting conn)
        {
            _conn = conn;
        }

        /// <summary>
        /// Add new Employee with the help of "Import_Employee_Cards" table
        /// </summary>
        /// <param name="_Employee"></param>
        public void AddEmployee(ImportEmployeeList _Employee)
        {
            try
            {
                //getMaxID() for geting a emloyee MaxID
                _Employee.CardID = "EMP00" + (getMaxID() + 1).ToString();

                //open myob file connection
                using (OdbcCommand _command = new OdbcCommand())
                {
                    _connectionCLS.ConnectionOpen(_conn);
                    string _query = string.Empty;
                    _query += " INSERT INTO Import_Employee_Cards  (CoLastName,CardStatus,CardID,Address1AddressLine1) VALUES(?,?,?,?) ";
                    //_query += "('" + _Employee.CoLastName + "','N','" + _Employee.CardID + "','" + _Employee.Address + "')";
                    _command.CommandText = _query;
                    _command.CommandType = CommandType.Text;
                    _command.Parameters.AddWithValue("@CoLastName", _Employee.CoLastName);
                    _command.Parameters.AddWithValue("@CardStatus", "N");
                    _command.Parameters.AddWithValue("@CardID", _Employee.CardID);
                    _command.Parameters.AddWithValue("@Address1AddressLine1", _Employee.Address);

                    _command.Connection = _connectionCLS.odbc;
                    _command.ExecuteNonQuery();
                    _command.CommandText = "END TRANSACTION";
                    _command.ExecuteNonQuery();

                    _command.Connection.Close();
                    _command.Connection.Dispose();

                    _connectionCLS.ConnectionClose();
                }

            }
            catch (Exception ex)
            {
               _connectionCLS.ConnectionClose();
                ErrorClass.WriteError("Error in EmployeeDAL class, function name AddEmployee[MyOB.DAL] :-" + ex.Message);
                throw;
            }

        }
        /// <summary>
        /// Check Employee in card table
        /// </summary>
        /// <param name="name">Pass employee last name</param>
        /// <returns>If employee exist then "true" otherwise "false" value</returns>
        public bool checkEmployeeIsExists(String name)
        {
            try
            {
              
                _connectionCLS.ConnectionOpen(_conn);
                using (OdbcCommand _command = new OdbcCommand("select Card.CardRecordID from Cards Card where(Card.CardTypeID='E' and Card.LastName='" + name + "')"))
                {
                    _connectionCLS.ConnectionOpen(_conn);
                    _command.Connection = _connectionCLS.odbc;
                    if (Convert.ToInt32(_command.ExecuteScalar()) > 0)
                    {
                        _connectionCLS.ConnectionClose();
                        return true;
                    }
                    else
                    {
                        _connectionCLS.ConnectionClose();
                        return false;
                    }
                   

                }

                //using (OdbcDataAdapter daCard = new OdbcDataAdapter("select Card.CardRecordID from Cards Card where(Card.CardTypeID='E' and Card.LastName='" + name + "')", _connectionCLS.odbc))
                //{
                    
                //    DataSet dsCard = new DataSet();
                //    daCard.Fill(dsCard);
                //    daCard.Dispose();
                //    _connectionCLS.ConnectionClose();
                //    if (dsCard != null && dsCard.Tables[0].Rows.Count > 0)
                //    {
                //        return true;
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                _connectionCLS.ConnectionClose();
                ErrorClass.WriteError("Error in EmployeeDAL class, function name checkEmployeeIsExists[MyOB.DAL] :-" + ex.Message);
                throw;
            }
        }
      /// <summary>
        ///  Get Max Employee id from "Employees" table
      /// </summary>
      /// <returns>Get Max Employee id</returns>
        private Int32 getMaxID()
        {
            try
            {
                Int32 CustomerID = 0;
                using (_command = new OdbcCommand("Select E.EmployeeID from  Employees E order by E.EmployeeID desc"))
                {
                    //Open myob file connection 
                    _connectionCLS.ConnectionOpen(_conn);
                    _command.Connection = _connectionCLS.odbc;
                    CustomerID = Convert.ToInt32(_command.ExecuteScalar());
                
                    //using (OdbcDataReader _dataReader = _command.ExecuteReader())
                    //{
                    //    Int32 CustomerID = 0;
                    //    int fCount = _dataReader.FieldCount;
                    //    while (_dataReader.Read())
                    //    {

                    //        CustomerID = Convert.ToInt32(_dataReader["EmployeeID"]);
                    //        break;
                    //    }
                    //    //close and dispose DataReader object
                    //    _dataReader.Close();
                    //    _dataReader.Dispose();
                    //    _connectionCLS.ConnectionClose();
                    //    return CustomerID;
                    //}
                }
                return CustomerID;
            }
            catch (Exception ex)
            {
                _connectionCLS.ConnectionClose();
                ErrorClass.WriteError("Error in EmployeeDAL class, function name getMaxID[MyOB.DAL] :-" + ex.Message);
                throw;
            }
        }
    }
}
