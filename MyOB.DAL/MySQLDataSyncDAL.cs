﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.Interface;
using System.Data.Odbc;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;
using System.Threading;
using ErrorLog;

namespace MyOB.DAL
{
  public  class MySQLDataSyncDAL
    {
       /// <summary>
       /// ConnectionCLS class object
       /// </summary>
        ConnectionCLS _connectionCLS = new ConnectionCLS();
      /// <summary>
      /// Odbc command object
      /// </summary>
        OdbcCommand _command = new OdbcCommand();
      /// <summary>
        /// MYOConnectionSetting class object for MYOB connection setting  
      /// </summary>
        MYOConnectionSetting _conn;
      /// <summary>
      /// For mysql connection string
      /// </summary>
        String _MySQlConn;
      /// <summary>
      /// object for Invoice Id 
      /// </summary>
        string InvoiceId = string.Empty;
      /// <summary>
      /// object for Invoice number
      /// </summary>
        string InvoiceNumber = string.Empty;
      /// <summary>
      /// object for Order Id
      /// </summary>
        string OrderId = "0";
      /// <summary>
      /// object for Order number
      /// </summary>
        string OrderNumber = string.Empty;
      /// <summary>
      /// object for GLN number
      /// </summary>
        string _GLNNumber = string.Empty;
      /// <summary>
      /// object for tax type like "GTN"
      /// </summary>
        string _tax = string.Empty;
      /// <summary>
      /// object for sales person detail
      /// </summary>
        string _salesPerson = string.Empty;
      /// <summary>
      /// object for ship via like "Air"
      /// </summary>
        string _shipVia = string.Empty;
      /// <summary>
      /// object for nurseries address
      /// </summary>
        string _address = string.Empty;
      /// <summary>
      /// object for myob log file path
      /// </summary>
        string _myObLogfile = string.Empty;

        int _assetAccount, _incomeAccount, _expenseAccount;
     
        string connectionResult=string.Empty;
        string _barcode = string.Empty;
        string _orderMessage = string.Empty;
        Int32 _totalOrderUpdate = 0;
        /// <summary>
        /// MySQLDataSyncDAL class constructor
        /// </summary>
        /// <param name="conn">Pass MyOB connection setting,for this is use MYOConnectionSetting class object</param>
        /// <param name="MySQlConn">MYSQL connection string for online portal databae </param>
        /// <param name="GLNNumber">pass nurseries GLNNumber</param>
        public MySQLDataSyncDAL(MYOConnectionSetting conn, String MySQlConn,string GLNNumber)
           {
               _conn = conn;
               _MySQlConn = MySQlConn;
           }

        //Import invoice from online to local database
      /// <summary>
      /// import Invoice from online portal to myob file
      /// </summary>
      /// <param name="localLOGfile">myob log file path</param>
        /// <param name="GLNNumber">nurseries gln number</param>
        /// <param name="tax">nurseries tax value</param>
        /// <param name="salesPerson">nurseries sales person value</param>
        /// <param name="shipVia">nurseries ship via value</param>
        /// <param name="address">nurseries address value</param>
      /// <returns>action status</returns>
        public string importInvoice(myobDataImportValues _myobDataImportValues,out string _message)
        {//pass value to local properties
            _GLNNumber = _myobDataImportValues.glnNumber;
            _tax = _myobDataImportValues.tax;
            _salesPerson = _myobDataImportValues.salesPerson;
            _shipVia = _myobDataImportValues.shipVia;
            _address = _myobDataImportValues.address;

            _myObLogfile = @"" + Path.GetDirectoryName(_myobDataImportValues.localLOGfile) + "\\{0}LOG.TXT";// File.ReadAllText(@"" + Path.GetDirectoryName(MYOBfilelocation) + "\\MYOBPLOG.TXT");
            _myObLogfile = string.Format(_myObLogfile, Path.GetFileNameWithoutExtension(_myobDataImportValues.MyoHostExePath));
            //account settings values 
            try
            {
                _assetAccount = _myobDataImportValues.AssestAccountItemInvento;
                _incomeAccount = _myobDataImportValues.IncomeAccountforTrackingSales;
                _expenseAccount =_myobDataImportValues.CostOfSaleAccount;
            }
            catch (Exception)
            {
                _message = "Please check your account setting values from setting screen.";
                return "0";
            }
            _connectionCLS.MySQlConnectionOpen(_MySQlConn);
              string _query = string.Empty;
              int ordersLoop = 0;

                    _command = new OdbcCommand();
                    //for storeing PurchaseOrderItems in dataset 
                    DataSet dsPurchaseOrderItems;
                    try
                    {

                        if (_connectionCLS.mySqlConn.State == System.Data.ConnectionState.Open)
                        {
                            // _connectionCLS.ConnectionOpen(_conn);

                            //count total orders from "purchase_orders" online portal database
                            using (MySqlCommand _mysqlCommand = new MySqlCommand("select count(*) as Total from purchase_orders where Status=1 and seller_gln='" + _GLNNumber + "'", _connectionCLS.mySqlConn))
                            {
                                MySqlDataReader _mysqlDataReader = _mysqlCommand.ExecuteReader();
                                while (_mysqlDataReader.Read())
                                {

                                    ordersLoop = Convert.ToInt32(_mysqlDataReader["Total"]) / 10;
                                    int remainderPart = Convert.ToInt32(_mysqlDataReader["Total"]) % 10;
                                    if (remainderPart > 0)
                                    {
                                        ordersLoop = ordersLoop + 1;
                                    }

                                    _mysqlDataReader.Close();
                                    _mysqlDataReader.Dispose();
                                    break;
                                }


                                //start loop on orders (Total Orders loop)
                                //##############################---######################
                                if (ordersLoop == 0)
                                {
                                    createLiveLog("0000000000", "(GLN: " + _GLNNumber + ") there is no records in order table for this " + _GLNNumber + " GLN number");
                                    _message = "There is no records in order table on your GLN number" + " (GLN: " + _GLNNumber + ")";
                                    return "0";
                                }
                                //ordersLoop
                                for (int k = 0; k < ordersLoop; k++)
                                {
                                    //select  count(*) from purchase_orders
                                    _query = string.Empty;
                                    _query = "select * from purchase_orders where status=1 and seller_gln='" + _GLNNumber + "' and id>'" + OrderId + "'  limit 10";

                                    using (MySqlDataAdapter dataAdapterPurchaseOrders = new MySqlDataAdapter(_query, _connectionCLS.mySqlConn))
                                    {
                                        DataSet dsPurchaseOrder = new DataSet();

                                        dataAdapterPurchaseOrders.Fill(dsPurchaseOrder);

                                        if (dsPurchaseOrder != null)
                                        {
                                            //##############################---######################
                                            //dsPurchaseOrder.Tables[0].Rows.Count
                                            for (int i = 0; i < dsPurchaseOrder.Tables[0].Rows.Count; i++)
                                            {
                                                //clear log file
                                                resetMyObLogFile(_myObLogfile);

                                                CustomersDAL _customers = new CustomersDAL(_conn, null);
                                                OrderId = dsPurchaseOrder.Tables[0].Rows[i]["id"].ToString();
                                                OrderNumber = dsPurchaseOrder.Tables[0].Rows[i]["purchase_order_number"].ToString();
                                                //################# check customer name exists or not
                                                if (!_customers.checkcustomerIsExists(dsPurchaseOrder.Tables[0].Rows[i]["bill_to_name"].ToString()))
                                                {
                                                    //Create new customer 
                                                    _customers.AddCustomer(new ImportCustomersList { CoLastName = dsPurchaseOrder.Tables[0].Rows[i]["bill_to_name"].ToString(), FirstName = dsPurchaseOrder.Tables[0].Rows[i]["bill_to_name"].ToString(), Address1AddressLine1 = dsPurchaseOrder.Tables[0].Rows[i]["bill_to_address"].ToString() });
                                                }
                                                //clear object from memory
                                                _customers = null;
                                                //################# check Sales Person exists or not
                                                EmployeeDAL _objEmployee = new EmployeeDAL(_conn);
                                                if (!_objEmployee.checkEmployeeIsExists(_salesPerson))
                                                {
                                                    //add new sales person 
                                                    _objEmployee.AddEmployee(new ImportEmployeeList { CoLastName = _salesPerson, Address = _address });
                                                }
                                                //clear object from memory
                                                _objEmployee = null;

                                                _query = string.Empty;
                                                _query += " select PRC.purchase_order_number,";
                                                _query += " PRC.id,";
                                                _query += " PRC.seller_gln,";
                                                _query += " PRC.bill_to_gln,";
                                                _query += " PRC.bill_to_name,";
                                                _query += " PRC.bill_to_address,";
                                                _query += " PRC.order_date,";
                                                _query += " PRC.deliver_date,";
                                                _query += " PRC.total_order_amount,";
                                                _query += " CUR.currency_code,";
                                                _query += " PRC.comments,";

                                                _query += " PRC.modified,";
                                                _query += " PRC.created,";

                                                _query += " POI.gtin as barcode, ";
                                                _query += " POI.description, ";
                                                _query += " POI.net_amount, ";
                                                _query += " POI.net_price, ";
                                                _query += " POI.requested_quantity, ";
                                                _query += " POI.bunnings_product_id, ";
                                                _query += " POI.name, ";
                                                _query += " POI.description, ";
                                                _query += " POI.net_price,";
                                                _query += " POI.net_amount,";
                                                _query += " POI.unit_of_measure,";
                                                _query += " POI.pack_quantity  ";

                                                _query += " from purchase_orders as PRC";
                                                _query += " inner join";

                                                _query += " currencies as CUR on CUR.id=PRC.currency_id and seller_gln='" + _GLNNumber + "' and PRC.status=1  ";
                                                _query += " inner join ";
                                                _query += " purchase_order_items as POI on POI.purchase_order_id=PRC.id and  POI.purchase_order_id='" + OrderId + "'";

                                                //fetch purchase order items data from mysql
                                                dsPurchaseOrderItems = new DataSet();

                                                using (MySqlDataAdapter dataAdapterPurchaseOrderItems = new MySqlDataAdapter(_query, _connectionCLS.mySqlConn))
                                                {
                                                    dataAdapterPurchaseOrderItems.Fill(dsPurchaseOrderItems);
                                                    dataAdapterPurchaseOrderItems.Dispose();
                                                    
                                                    //ds.Tables[0].Rows.Count

                                                    ItemsDAL _objItem = new ItemsDAL(_conn);
                                                    string _itemNumber = string.Empty;
                                                    
                                                    //##############################---######################
                                                    //dsPurchaseOrderItems.Tables[0].Rows.Count
                                                    _query = string.Empty;
                                                    _query += "(ItemNumber,CoLastName,ExTaxPrice,IncTaxPrice,ExTaxTotal,IncTaxTotal,DeliveryStatus,Quantity,TaxCode,FreightTaxCode,PaymentIsDue,DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,SaleStatus,SaleDate,SalespersonLastName,Comment,CustomersNumber,ShipVia,FreightGSTAmount) VALUES  (";
                                                    for (int j = 0; j < dsPurchaseOrderItems.Tables[0].Rows.Count; j++)
                                                    {
                                                        _itemNumber = string.Empty;
                                                        _barcode = string.Empty;
                                                        _barcode = dsPurchaseOrderItems.Tables[0].Rows[j]["barcode"].ToString();
                                                        //Add 0 prefix to gtin if gtin < 14 digits
                                                        if (_barcode != string.Empty)
                                                        {
                                                            _barcode = ("00000000000000" + _barcode).Substring(_barcode.Length);
                                                            dsPurchaseOrderItems.Tables[0].Rows[j]["barcode"] = _barcode;
                                                        }
                                                        // on the base of barcode get item from item table 
                                                        _itemNumber = _objItem.GetItemNumber(_barcode);
                                                       
                                                        ItemsDAL _itemDAL = new ItemsDAL(_conn);

                                                        //update ann add new item in myob database
                                                         ItemsList _Itemlist = new ItemsList();
                                                        if (_itemNumber == string.Empty)
                                                        {
                                                            //Create item for this call AddItem function from Items DAL
                                                            _Itemlist.ItemNumber = "P0" + _itemDAL.GetMaxItemID().ToString();
                                                            //CreateLiveLog(OrderNumber, dsPurchaseOrderItems.Tables[0].Rows[j]["barcode"].ToString() + " item barcode not exist in myob database");
                                                        }
                                                        else
                                                        {
                                                            _Itemlist.ItemNumber = _itemNumber;
                                                        }
                                                       
                                                        _Itemlist.ItemName = dsPurchaseOrderItems.Tables[0].Rows[j]["name"].ToString();
                                                        _Itemlist.SellUnitMeasure = dsPurchaseOrderItems.Tables[0].Rows[j]["unit_of_measure"].ToString();
                                                        _Itemlist.Description = dsPurchaseOrderItems.Tables[0].Rows[j]["description"].ToString();
                                                        _Itemlist.SellingPrice = Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString());
                                                        _Itemlist.StandardCost = Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString());
                                                        _Itemlist.Quantity = Convert.ToInt32(dsPurchaseOrderItems.Tables[0].Rows[j]["requested_quantity"].ToString());
                                                        _Itemlist.TaxCodeWhenBought = _tax.Trim();
                                                        _Itemlist.Barcode = _barcode;
                                                        _Itemlist.Memo = "NA";
                                                        _Itemlist.AssetAccount = _assetAccount;
                                                        _Itemlist.IncomeAccount = _incomeAccount;
                                                        _Itemlist.ExpenseAccount = _expenseAccount;

                                                        if (_Itemlist.ItemName == string.Empty)
                                                        {
                                                            createLiveLog(OrderNumber, "Barcode" + _barcode + ", item name does not exist in purchase_order_items table");
                                                            _message = "item name does not exist in purchase_order_items table, order number is " + OrderNumber + " and bar code is " + _barcode;
                                                            return "0";
                                                        }
                                                        else
                                                        {
                                                            _itemDAL.AddItem(_Itemlist);
                                                        }

                                                        _itemNumber = _objItem.GetItemNumber(_barcode);
                                                        //InvoiceNumber
                                                        
                                                        if (_itemNumber == string.Empty)
                                                              {
                                                                  createLiveLog(OrderNumber, readMYOBLogFile(_myObLogfile));
                                                                  _message = "Item not created on " + _barcode + " bar code." + Environment.NewLine;
                                                                  return "0";
                                                                  //continue;
                                                              }
                                                              else
                                                              {
                                                                  _query += "('" + _itemNumber + "',";
                                                                  _query += "'" + dsPurchaseOrderItems.Tables[0].Rows[j]["bill_to_name"].ToString() + "',";
                                                                  _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString()) + ",";
                                                                  _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString()) + ",";
                                                                  _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_amount"].ToString()) + ",";
                                                                  _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_amount"].ToString()) + ",";

                                                                  _query += "'A',";
                                                                  //_query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["pack_quantity"].ToString()) + ",";
                                                                  _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["requested_quantity"].ToString()) + ",";
                                                                  //GSTAmount
                                                                 // _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString()) + ",";
                                                                  _query += "'" + _tax + "',";
                                                                  _query += "'" + _tax + "',";
                                                                  _query += "0,0,0,0,0,'I',";
                                                                  _query += "'" + Common.setDateformat(dsPurchaseOrderItems.Tables[0].Rows[j]["order_date"].ToString()) + "',";
                                                                  _query += "'" + _salesPerson + "',";
                                                                  _query += "'" + dsPurchaseOrderItems.Tables[0].Rows[j]["comments"].ToString().Replace("'"," ") + "',";
                                                                  _query += "'" + dsPurchaseOrderItems.Tables[0].Rows[j]["purchase_order_number"].ToString() + "',";

                                                                  decimal taxCalulated = (Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["total_order_amount"].ToString()) / 100) * 10;

                                                                  _query += "'" + _shipVia + "'," + taxCalulated + ")";

                                                                  if (dsPurchaseOrderItems.Tables[0].Rows.Count > 1 && dsPurchaseOrderItems.Tables[0].Rows.Count - 1 != j)
                                                                  {
                                                                      _query += " , ";
                                                                  }
                                                              }
                                                        //_query += "('110','Cash Sales',33.99,33.99,135.95,149.56,'A',1.0,33.99,'GST',0,0,0,0,0,'I','" + Common.setDateformat(dsPurchaseOrderItems.Tables[0].Rows[i]["order_date"].ToString()) + "','Sales Man','Comment','" + dsPurchaseOrderItems.Tables[0].Rows[0]["purchase_order_number"].ToString() + "','Air')";
                                                        // "INSERT INTO Import_NonConsolidated_TaxCodes(TaxCode,Description,TaxType,Rate) VALUES('TGT','BAT',2,10)";

                                                    }//End dsPurchaseOrderItems loop
                                                    //00000044
                                                    _query += ")";

                                                    using (OdbcCommand command = new OdbcCommand())
                                                    {
                                                        try
                                                        {
                                                            command.CommandText = "INSERT INTO Import_Item_Sales " + _query;
                                                            _connectionCLS.ConnectionOpen(_conn);
                                                            command.Connection = _connectionCLS.odbc;
                                                            var _ids = command.ExecuteNonQuery();

                                                            command.CommandText = "END TRANSACTION";
                                                            var _id = command.ExecuteNonQuery();
                                                            _connectionCLS.ConnectionClose();

                                                          
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            _command.Connection.Close();
                                                            _connectionCLS.ConnectionClose();
                                                            _connectionCLS.MySQlConnectionClose();

                                                            ErrorClass.WriteError("Error in importInvoice function on Import_Item_Sales :-" + ex.Message);
                                                        }
                                                        //   Thread.Sleep(2000);
                                                    }
                                                }//End dataAdapterPurchaseOrderItems using statement 


                                                //##########################-Update Invoice data on live database-####################
                                                //OrderNumber=4121S30323221
                                                //Check Invoice Already Exist Or Not mysql invoice table
                                                DataTable _dtInvoice = checkMYOBInvoiceStatus(OrderNumber);
                                                if (!checkInvoiceAlreadyExistOrNot())
                                                {
                                                    //start update invoice in online database
                                                    updateInvoice(_dtInvoice, OrderId, dsPurchaseOrderItems.Tables[0]);
                                                }

                                                if (_dtInvoice.Rows.Count > 0)
                                                {
                                                    _totalOrderUpdate++;
                                                    //update order's status on live database 
                                                    updateOrderStatusOnMySqlDatabase(OrderId);
                                                    //create Log in my sql database
                                                    String _myobmessgae = readMYOBLogFile(_myObLogfile);
                                                    if (_myobmessgae != string.Empty && _myobmessgae!=null)
                                                    {
                                                        createLiveLog(OrderNumber,_myobmessgae);
                                                    }
                                                    else
                                                    {
                                                        createLiveLog(OrderNumber, "records imported without errors.");
                                                    }
                                                }
                                                _dtInvoice = null;
                                                dsPurchaseOrderItems = null;
                                                //Write invoice tracking
                                                //ErrorLog.TrackInvoiceLog.WriteInvoiceLog( ds.Tables[0].Rows[0]["purchase_order_number"].ToString() + "," + InvoiceNumber + "," + DateTime.UtcNow.ToString());
                                            }//End dsPurchaseOrder loop

                                        }//End if for Check "dsPurchaseOrder" dataset null
                                       
                                    }//End dataAdapterPurchaseOrders using statement 

                                }//End total Order loop
                            }//End Total Orders using statement 
                            if (_totalOrderUpdate == 1)
                            {
                                _orderMessage = _totalOrderUpdate.ToString() + " invoice created in myob." + Environment.NewLine;
                            }
                            else if (_totalOrderUpdate > 1)
                            {
                                _orderMessage = _totalOrderUpdate.ToString() + " invoices created in myob." + Environment.NewLine;
                            }
                            _message = _orderMessage+ " Invoice import and export completed";
                            return "0";

                        }//End if for check mysql connection open or not
                        else {
                            _message = "right now online portal database not live, please try again later ";
                            return "1";
                        }
            }
            catch (Exception ex)
            {
                //myTrans.Rollback();
            
                _connectionCLS.odbc.Dispose();
                _connectionCLS.MySQlConnectionClose();
                ErrorClass.WriteError("Error in importInvoice function on main try block [MyOB.DAL]:-" + ex.Message);
                _message = ex.Message;
                return ex.Message;
            }
        }
      /// <summary>
      /// Get updated invoice from online portal and update invoice in myob database
      /// </summary>
      /// <param name="_myobDataImportValues"></param>
      /// <param name="_message"></param>
      /// <returns></returns>
        public string importUpdatedInvoice(myobDataImportValues _myobDataImportValues, out string _message)
        {
            //pass value to local properties
            _GLNNumber = _myobDataImportValues.glnNumber;
            _tax = _myobDataImportValues.tax;
            _salesPerson = _myobDataImportValues.salesPerson;
            _shipVia = _myobDataImportValues.shipVia;
            _address = _myobDataImportValues.address;
          string  InvoiceId = "0";
            _myObLogfile = @"" + Path.GetDirectoryName(_myobDataImportValues.localLOGfile) + "\\MYOBPLOG.TXT";// File.ReadAllText(@"" + Path.GetDirectoryName(MYOBfilelocation) + "\\MYOBPLOG.TXT");
            //account settings values 
            try
            {
                _assetAccount = _myobDataImportValues.AssestAccountItemInvento;
                _incomeAccount = _myobDataImportValues.IncomeAccountforTrackingSales;
                _expenseAccount = _myobDataImportValues.CostOfSaleAccount;
            }
            catch (Exception)
            {
                _message = "Please check your account setting values from setting screen.";
                return "0";
            }
            _connectionCLS.MySQlConnectionOpen(_MySQlConn);
            string _query = string.Empty;
            int ordersLoop = 0;

            _command = new OdbcCommand();
            //for storeing PurchaseOrderItems in dataset 
            DataSet dsPurchaseOrderItems;
            try
            {

                if (_connectionCLS.mySqlConn.State == System.Data.ConnectionState.Open)
                {
                    // _connectionCLS.ConnectionOpen(_conn);

                    //count total orders from "purchase_orders" online portal database
                    //and nursery_gln='" + _GLNNumber + "'
                    using (MySqlCommand _mysqlCommand = new MySqlCommand("select count(*) as Total from invoices where update_for_myob='1' and nursery_gln='" + _GLNNumber + "'", _connectionCLS.mySqlConn))
                    {
                        MySqlDataReader _mysqlDataReader = _mysqlCommand.ExecuteReader();
                        while (_mysqlDataReader.Read())
                        {

                            ordersLoop = Convert.ToInt32(_mysqlDataReader["Total"]) / 10;
                            int remainderPart = Convert.ToInt32(_mysqlDataReader["Total"]) % 10;
                            if (remainderPart > 0)
                            {
                                ordersLoop = ordersLoop + 1;
                            }

                            _mysqlDataReader.Close();
                            _mysqlDataReader.Dispose();
                            break;
                        }


                        //start loop on orders (Total Orders loop)
                        //##############################---######################
                        if (ordersLoop == 0)
                        {
                            createLiveLog("0000000000", "(GLN: " + _GLNNumber + ") there is no records in order table for this " + _GLNNumber + " GLN number");
                            _message = "There is no records for update in invoices table on your GLN number" + " (GLN: " + _GLNNumber + ")";
                            return "0";
                        }

                        string _invoice_number = string.Empty;
                        //ordersLoop
                        for (int k = 0; k < ordersLoop; k++)
                        {
                            //select  count(*) from purchase_orders
                            //and nursery_gln='" + _GLNNumber + "'
                            _query = string.Empty;
                            _query = "select id,purchase_order_number,invoice_number from invoices where update_for_myob='1'  and id>'" + InvoiceId + "' and nursery_gln='" + _GLNNumber + "'  limit 10";

                            using (MySqlDataAdapter dataAdapterPurchaseOrders = new MySqlDataAdapter(_query, _connectionCLS.mySqlConn))
                            {
                                DataSet dsPurchaseOrder = new DataSet();

                                dataAdapterPurchaseOrders.Fill(dsPurchaseOrder);

                                if (dsPurchaseOrder != null)
                                {
                                    //##############################---######################
                                    //dsPurchaseOrder.Tables[0].Rows.Count
                                    for (int i = 0; i < dsPurchaseOrder.Tables[0].Rows.Count; i++)
                                    {
                                        //clear log file
                                        resetMyObLogFile(_myObLogfile);

                                        CustomersDAL _customers = new CustomersDAL(_conn, null);
                                        InvoiceId = dsPurchaseOrder.Tables[0].Rows[i]["id"].ToString();
                                        OrderNumber = dsPurchaseOrder.Tables[0].Rows[i]["purchase_order_number"].ToString();
                                        _invoice_number = dsPurchaseOrder.Tables[0].Rows[i]["invoice_number"].ToString();
                                        DataTable _datatTableInvoice = getMYOBInvoice(_invoice_number);
                                        //################# check customer name exists or not
                                        //if (!_customers.checkcustomerIsExists(dsPurchaseOrder.Tables[0].Rows[i]["bill_to_name"].ToString()))
                                        //{
                                        //    //Create new customer 
                                        //    _customers.AddCustomer(new ImportCustomersList { CoLastName = dsPurchaseOrder.Tables[0].Rows[i]["bill_to_name"].ToString(), FirstName = dsPurchaseOrder.Tables[0].Rows[i]["bill_to_name"].ToString(), Address1AddressLine1 = dsPurchaseOrder.Tables[0].Rows[i]["bill_to_address"].ToString() });
                                        //}
                                        ////clear object from memory
                                        //_customers = null;
                                        //################# check Sales Person exists or not
                                        EmployeeDAL _objEmployee = new EmployeeDAL(_conn);
                                        if (!_objEmployee.checkEmployeeIsExists(_salesPerson))
                                        {
                                            //add new sales person 
                                            _objEmployee.AddEmployee(new ImportEmployeeList { CoLastName = _salesPerson, Address = _address });
                                        }
                                        //clear object from memory
                                        _objEmployee = null;
                                        _query = string.Empty;
                                        _query += " select ";
                                        _query += " INVO.ID,INVO.invoice_number,";
                                        _query += " INVO.purchase_order_id,";
                                        _query += " INVO.invoice_type,";
                                        _query += " INVO.purchase_order_number,";
                                        _query += " INVO.sub_total,";
                                        _query += " INVO.tax,";
                                        _query += " INVO.sales_person,";
                                        _query += " INVO.ship_via,";
                                        _query += " INVO.update_for_myob,";

                                        _query += " invItem.order_line_number,";
                                        _query += " invItem.gtin as barcode,";
                                        _query += " invItem.supplier_product_id,";
                                        _query += " invItem.bunnings_product_id,";
                                        _query += " invItem.requested_quantity,";
                                        _query += " invItem.unit_of_measure,";
                                        _query += " invItem.pack_quantity,";
                                        _query += " invItem.quotation_number,";
                                        _query += " invItem.description,";
                                        _query += " invItem.net_price,";
                                        _query += " invItem.net_amount,";
                                        _query += " invItem.reference_purchase_order_line_number,";
                                        _query += " invItem.gst_amount,";
                                        _query += " invItem.gst_percentage,";
                                        _query += " invItem.comments,";
                                        _query += " invItem.name,";
                                        _query += " invItem.genus,";
                                        _query += " invItem.species,";
                                        _query += " invItem.cultival,";
                                        _query += " invItem.pot_size";

                                        _query += " from invoices as INVO";
                                        _query += " inner join ";
                                        _query += " invoice_items as invItem on invItem.invoice_id=INVO.id and  INVO.update_for_myob='1' and INVO.id=" + InvoiceId;
 



                                        //fetch purchase order items data from mysql
                                        dsPurchaseOrderItems = new DataSet();

                                        using (MySqlDataAdapter dataAdapterPurchaseOrderItems = new MySqlDataAdapter(_query, _connectionCLS.mySqlConn))
                                        {
                                            dataAdapterPurchaseOrderItems.Fill(dsPurchaseOrderItems);
                                            dataAdapterPurchaseOrderItems.Dispose();

                                            //ds.Tables[0].Rows.Count

                                            ItemsDAL _objItem = new ItemsDAL(_conn);
                                            string _itemNumber = string.Empty;

                                            //##############################---######################
                                            //dsPurchaseOrderItems.Tables[0].Rows.Count
                                            _query = string.Empty;
                                            _query += "(ItemNumber,ExTaxPrice,IncTaxPrice,ExTaxTotal,IncTaxTotal,DeliveryStatus,Quantity,TaxCode,PaymentIsDue,DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,SaleStatus,SalespersonLastName,Comment,CustomersNumber,ShipVia,InvoiceNumber,RecordID) VALUES  (";
                                            for (int j = 0; j < dsPurchaseOrderItems.Tables[0].Rows.Count; j++)
                                            {
                                                _itemNumber = string.Empty;
                                                _barcode = string.Empty;
                                                _barcode = dsPurchaseOrderItems.Tables[0].Rows[j]["barcode"].ToString();
                                                //Add 0 prefix to gtin if gtin < 14 digits
                                                if (_barcode != string.Empty)
                                                {
                                                    _barcode = ("00000000000000" + _barcode).Substring(_barcode.Length);
                                                    dsPurchaseOrderItems.Tables[0].Rows[j]["barcode"] = _barcode;
                                                }
                                                // on the base of barcode get item from item table 
                                                _itemNumber = _objItem.GetItemNumber(_barcode);

                                                ItemsDAL _itemDAL = new ItemsDAL(_conn);

                                                //update ann add new item in myob database
                                                ItemsList _Itemlist = new ItemsList();
                                                if (_itemNumber == string.Empty)
                                                {
                                                    //Create item for this call AddItem function from Items DAL
                                                    _Itemlist.ItemNumber = "P0" + _itemDAL.GetMaxItemID().ToString();
                                                    //CreateLiveLog(OrderNumber, dsPurchaseOrderItems.Tables[0].Rows[j]["barcode"].ToString() + " item barcode not exist in myob database");
                                                }
                                                else
                                                {
                                                    _Itemlist.ItemNumber = _itemNumber;
                                                }

                                                _Itemlist.ItemName = dsPurchaseOrderItems.Tables[0].Rows[j]["name"].ToString();
                                                _Itemlist.SellUnitMeasure = dsPurchaseOrderItems.Tables[0].Rows[j]["unit_of_measure"].ToString();
                                                _Itemlist.Description = dsPurchaseOrderItems.Tables[0].Rows[j]["description"].ToString();
                                                _Itemlist.SellingPrice = Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString());
                                                _Itemlist.StandardCost = Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString());
                                                _Itemlist.Quantity = Convert.ToInt32(dsPurchaseOrderItems.Tables[0].Rows[j]["requested_quantity"].ToString());
                                                _Itemlist.TaxCodeWhenBought = _tax.Trim();
                                                _Itemlist.Barcode = _barcode;
                                                _Itemlist.Memo = dsPurchaseOrderItems.Tables[0].Rows[j]["comments"].ToString();
                                                _Itemlist.AssetAccount = _assetAccount;
                                                _Itemlist.IncomeAccount = _incomeAccount;
                                                _Itemlist.ExpenseAccount = _expenseAccount;

                                                if (_Itemlist.ItemName == string.Empty)
                                                {
                                                    createLiveLog(OrderNumber, "Barcode" + _barcode + ", item name does not exist in invoices table");
                                                    _message = "item name does not exist in invoices table, order number is " + OrderNumber + " and bar code is " + _barcode;
                                                    return "0";
                                                }
                                                else
                                                {
                                                    _itemDAL.AddItem(_Itemlist);
                                                }


                                                _itemNumber = _objItem.GetItemNumber(_barcode);
                                                //InvoiceNumber

                                                if (_itemNumber == string.Empty)
                                                {
                                                    createLiveLog(OrderNumber, readMYOBLogFile(_myObLogfile));
                                                    _message = "Item not created on " + _barcode + " bar code." + Environment.NewLine;
                                                    return "0";
                                                    //continue;
                                                }
                                                else
                                                {
                                                    //****get updated invoice detail from myob sale table invoice_number*************
                                                    _invoice_number = dsPurchaseOrderItems.Tables[0].Rows[j]["invoice_number"].ToString();
                                                    
                                                    if (_datatTableInvoice.Rows.Count > 0)
                                                    {
                                                        _query += "('" + _itemNumber + "',";
                                                        //CoLastName
                                                        //  _query += "'" + _dtInvoice.Rows[0][""].ToString() + "',";
                                                        _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString()) + ",";
                                                        _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString()) + ",";
                                                        _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_amount"].ToString()) + ",";
                                                        _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_amount"].ToString()) + ",";

                                                        _query += "'A',";
                                                        //_query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["pack_quantity"].ToString()) + ",";
                                                        _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["requested_quantity"].ToString()) + ",";
                                                        //GSTAmount
                                                        // _query += "" + Convert.ToDecimal(dsPurchaseOrderItems.Tables[0].Rows[j]["net_price"].ToString()) + ",";
                                                        _query += "'" + _tax + "',";
                                                        _query += "0,0,0,0,0,'I',";
                                                       // _query += "'" + Common.setDateformat("2015-09-22") + "',";
                                                        _query += "'" + _salesPerson + "',";
                                                        _query += "'" + dsPurchaseOrderItems.Tables[0].Rows[j]["comments"].ToString() + "',";
                                                        _query += "'" + dsPurchaseOrderItems.Tables[0].Rows[j]["purchase_order_number"].ToString() + "',";
                                                        _query += "'" + _shipVia + "','" + _invoice_number + "'," + _datatTableInvoice.Rows[0]["CardRecordID"] + ")";

                                                        if (dsPurchaseOrderItems.Tables[0].Rows.Count > 1 && dsPurchaseOrderItems.Tables[0].Rows.Count - 1 != j)
                                                        {
                                                            _query += " , ";
                                                        }


                                                    }
                                                    //_query += "('110','Cash Sales',33.99,33.99,135.95,149.56,'A',1.0,33.99,'GST',0,0,0,0,0,'I','" + Common.setDateformat(dsPurchaseOrderItems.Tables[0].Rows[i]["order_date"].ToString()) + "','Sales Man','Comment','" + dsPurchaseOrderItems.Tables[0].Rows[0]["purchase_order_number"].ToString() + "','Air')";
                                                    // "INSERT INTO Import_NonConsolidated_TaxCodes(TaxCode,Description,TaxType,Rate) VALUES('TGT','BAT',2,10)";
                                                }
                                            }//End dsPurchaseOrderItems loop
                                            //00000044
                                            _query += ")";

                                            using (OdbcCommand command = new OdbcCommand())
                                            {
                                                try
                                                {
                                                    command.CommandText = "INSERT INTO Import_Item_Sales " + _query;
                                                    _connectionCLS.ConnectionClose();
                                                    _connectionCLS.ConnectionOpen(_conn);
                                                    command.Connection = _connectionCLS.odbc;
                                                    var _ids = command.ExecuteNonQuery();
                                                    command.Connection.Close();
                                                using(OdbcCommand    command1= new OdbcCommand()){
                                                    _connectionCLS.ConnectionOpen(_conn);
                                                    command1.Connection = _connectionCLS.odbc;
                                                    command1.CommandText = "UPDATE DUPLICATES";
                                                    var _id = command1.ExecuteNonQuery();
                                                    command.Connection.Close();
                                                   
                                                      }
                                                using (OdbcCommand command2 = new OdbcCommand())
                                                {
                                                    _connectionCLS.ConnectionOpen(_conn);
                                                    command2.Connection = _connectionCLS.odbc;
                                                    command2.CommandText = "END TRANSACTION";
                                                    var _id = command2.ExecuteNonQuery();
                                                    command.Connection.Close();
                                                }
                                                _connectionCLS.ConnectionClose();
                                                  

                                                   
                                                }
                                                catch (Exception ex)
                                                {
                                                  
                                                    _connectionCLS.ConnectionClose();
                                                    _connectionCLS.MySQlConnectionClose();

                                                    ErrorClass.WriteError("Error in importUpdatedInvoice function on Import_Item_Sales :-" + ex.Message);
                                                }
                                                //   Thread.Sleep(2000);
                                            }
                                        }//End dataAdapterPurchaseOrderItems using statement 


                                        //##########################-Update Invoice data on live database-####################
                                        //OrderNumber=4121S30323221
                                        //Check Invoice Already Exist Or Not mysql invoice table
                                       // DataTable _dtInvoice = checkMYOBInvoiceStatus(OrderNumber);
                                        //if (!checkInvoiceAlreadyExistOrNot())
                                        //{
                                        //    //start update invoice in online database
                                        //    updateInvoice(_dtInvoice, OrderId, dsPurchaseOrderItems.Tables[0]);
                                        //}
                                        if (_datatTableInvoice.Rows.Count > 0)
                                        {
                                            _totalOrderUpdate++;
                                            //update order's status on live database 
                                          //  updateOrderStatusOnMySqlDatabase(OrderId);
                                            //create Log in my sql database
                                            String _myobmessgae = readMYOBLogFile(_myObLogfile);
                                            if (_myobmessgae != string.Empty && _myobmessgae!=null)
                                            {
                                                createLiveLog(OrderNumber, readMYOBLogFile(_myObLogfile));
                                            }
                                            else
                                            {
                                                createLiveLog(OrderNumber, "invoice updated without errors.");
                                            }
                                        }
                                        _datatTableInvoice = null;
                                        dsPurchaseOrderItems = null;
                                        //Write invoice tracking
                                        //ErrorLog.TrackInvoiceLog.WriteInvoiceLog( ds.Tables[0].Rows[0]["purchase_order_number"].ToString() + "," + InvoiceNumber + "," + DateTime.UtcNow.ToString());
                                    }//End dsPurchaseOrder loop

                                }//End if for Check "dsPurchaseOrder" dataset null

                            }//End dataAdapterPurchaseOrders using statement 

                        }//End total Order loop
                    }//End Total Orders using statement 
                    if (_totalOrderUpdate == 1)
                    {
                        _orderMessage = _totalOrderUpdate.ToString() + " invoice updated in myob." + Environment.NewLine;
                    }
                    else if (_totalOrderUpdate > 1)
                    {
                        _orderMessage = _totalOrderUpdate.ToString() + " invoices updated in myob." + Environment.NewLine;
                    }
                    _message = _orderMessage + ". Updated invoices import and export completed";
                    return "0";

                }//End if for check mysql connection open or not
                else
                {
                    _message = "right now online portal database not live, please try again later ";
                    return "1";
                }
            }
            catch (Exception ex)
            {
                //myTrans.Rollback();

                _connectionCLS.odbc.Dispose();
                _connectionCLS.MySQlConnectionClose();
                ErrorClass.WriteError("Error in importInvoice function on main try block [MyOB.DAL]:-" + ex.Message);
                _message = ex.Message;
                return ex.Message;
            }
        }

        /// <summary>
        /// update invoice number on live database(MYSQL)
        /// </summary>
        private void updateInvoice(DataTable saleDatatable, string OrderId,DataTable onlineOrderItemsDatatable)
        {
            try
            {
                if (saleDatatable.Rows.Count > 0)
                {
                    
                   
                    _connectionCLS.MySQlConnectionOpen(_MySQlConn);

                    string InvoiceType = string.Empty;
                    if (_connectionCLS.mySqlConn.State == System.Data.ConnectionState.Open)
                    {
                        switch (saleDatatable.Rows[0]["InvoiceTypeID"].ToString())
                        {
                            case "S":
                                InvoiceType = "Service";
                                break;
                            case "I":
                                InvoiceType = "TAX_INVOICE";
                                break;
                            case "P":
                                InvoiceType = "Professional";
                                break;
                            case "M":
                                InvoiceType = "Time Billing";
                                break;
                            default:
                                InvoiceType = "No Default";
                                break;
                        }

                        InvoiceNumber = saleDatatable.Rows[0]["InvoiceNumber"].ToString();
                        string InvoiceID = string.Empty;
                        string _query = "insert into invoices ";
                        _query += "(purchase_order_id,invoice_number,invoice_type,purchase_order_number,tax,sub_total,sales_person,ship_via,nursery_gln,created) value ";
                        _query += "('" + OrderId + "', ";
                        _query += "'" + InvoiceNumber + "', ";
                        _query += "'" + InvoiceType + "', ";
                        _query += "'" + OrderNumber + "', ";
                    //    _query += "'" + saleDatatable.Rows[0]["InvoiceDeliveryID"].ToString() + "', ";
                        _query += "'" + saleDatatable.Rows[0]["TotalTax"].ToString() + "', ";
                        _query += "'" + saleDatatable.Rows[0]["TotalLines"].ToString() + "', ";
                        _query += "'" + _salesPerson + "', ";
                        _query += "'" + _shipVia + "', ";
                        _query += "'" + _GLNNumber + "', ";
                        
                        _query += "'" + Common.mysqlDateformat(DateTime.Now.ToString()) + "') ";

                        using (MySqlCommand _mysqlCommand = new MySqlCommand(_query, _connectionCLS.mySqlConn))
                        {
                            _mysqlCommand.ExecuteNonQuery();
                            _mysqlCommand.Dispose();
                        }

                        using (MySqlCommand _mysqlCommand = new MySqlCommand("select id from invoices where invoice_number='" + InvoiceNumber + "'", _connectionCLS.mySqlConn))
                        {
                            MySqlDataReader _mysqlDataReader = _mysqlCommand.ExecuteReader();
                            while (_mysqlDataReader.Read())
                            {

                                InvoiceID = _mysqlDataReader["id"].ToString();

                                //rest objects
                                _mysqlDataReader.Close();
                                _mysqlDataReader.Dispose();

                                break;
                            }
                        }

                        //get invoice item detail from myob database   
                        DataTable invoiceDataTable = getMYOBInvoiceItems(saleDatatable.Rows[0]["SaleID"].ToString());

                        for (int j = 0; j < invoiceDataTable.Rows.Count; j++)
                        {
                            DataRow[] _row;

                            _row = onlineOrderItemsDatatable.Select("barcode='" + invoiceDataTable.Rows[j]["CustomField1"].ToString() + "'");
                            if (_row.Length == 0)
                            {
                                if (_row.Length == 0)
                                {
                                    _row = onlineOrderItemsDatatable.Select("barcode='" + invoiceDataTable.Rows[j]["CustomField2"].ToString() + "'");

                                    if (_row.Length == 0)
                                    {
                                        _row = onlineOrderItemsDatatable.Select("barcode='" + invoiceDataTable.Rows[j]["CustomField3"].ToString() + "'");
                                    }
                                }
                            }


                            _query = string.Empty;
                            _query = "insert into invoice_items";
                            _query += "(invoice_id,purchase_order_id,order_line_number,gtin,supplier_product_id,bunnings_product_id,requested_quantity,unit_of_measure,pack_quantity,quotation_number,description,net_price,net_amount,reference_purchase_order_line_number,gst_amount,gst_percentage,comments,name,created) values";
                            _query += "('" + InvoiceID + "', ";
                            _query += "'" + OrderId + "', ";
                            _query += "'" + invoiceDataTable.Rows[j]["LineNumber"] + "', ";
                            _query += "'" + _row[0]["barcode"] + "', ";
                            _query += "'" + invoiceDataTable.Rows[j]["ItemID"] + "', ";
                            _query += "'" + _row[0]["bunnings_product_id"] + "', ";
                            _query += "'" + _row[0]["requested_quantity"] + "', ";
                            _query += "'" + invoiceDataTable.Rows[j]["SellUnitMeasure"] + "', ";
                            _query += "'" + invoiceDataTable.Rows[j]["Quantity"] + "', ";
                            _query += "'', ";
                            _query += "'" + invoiceDataTable.Rows[j]["Description"] + "', ";
                            _query += "'" + invoiceDataTable.Rows[j]["TaxInclusiveUnitPrice"] + "', ";
                            _query += "'" + invoiceDataTable.Rows[j]["TaxInclusiveTotal"] + "', ";
                            _query += "'', ";

                            decimal gst_amount = (Convert.ToDecimal(invoiceDataTable.Rows[j]["TaxInclusiveTotal"]) / 100) * 10;
                            _query += "'" + gst_amount.ToString("#.##") + "', ";
                            // decimal gst_percentage = ((gst_amount / Convert.ToDecimal(invoiceDataTable.Rows[j]["TaxInclusiveUnitPrice"])) * 100);
                            _query += "'10', ";
                            _query += "'" + saleDatatable.Rows[0]["Comment"].ToString() + "', ";
                            _query += "'" + _row[0]["name"] + "', ";
                            _query += "'" + Common.mysqlDateformat(DateTime.Now.ToString()) + "') ";

                            using (MySqlCommand _mysqlCommand = new MySqlCommand(_query, _connectionCLS.mySqlConn))
                            {
                                _mysqlCommand.ExecuteNonQuery();
                                _mysqlCommand.Dispose();
                            }

                                    _query = null;

                        }
                    

                    }

                }//end if no invoice added in myob 
                else
                {
                    createLiveLog(OrderNumber, readMYOBLogFile(_myObLogfile));
                }
            }
            catch (Exception ex)
            {
                //createLiveLog(OrderNumber, ex.Message);
                createLiveLog(OrderNumber, readMYOBLogFile(_myObLogfile));
                _connectionCLS.mySqlConn.Close();
                _connectionCLS.mySqlConn.Dispose();

                ErrorClass.WriteError("Error in MYSQLDataSync class, function name updateInvoice [MyOB.DAL]:-" + ex.Message); 
            }
        }
      /// <summary>
      /// Check invoice number in online database exist ot not
      /// </summary>
      /// <returns></returns>
        private bool checkInvoiceAlreadyExistOrNot()
        {//purchase_order_number
            try
            {
                Int32 _count;
                using (MySqlCommand _mysqlCommand = new MySqlCommand("select COUNT(*)as Total FROM invoices where purchase_order_number='" + OrderNumber + "'", _connectionCLS.mySqlConn))
                {
                    _count = Convert.ToInt32(_mysqlCommand.ExecuteScalar());
                    _mysqlCommand.Dispose();
                }
                if (_count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
      /// <summary>
      /// Get invoice items from myob database
      /// </summary>
      /// <param name="saleID"></param>
        /// <returns>Invoice Items in DataTable </returns>
        private DataTable getMYOBInvoiceItems(string saleID)
        {
            try
            {
                _connectionCLS.ConnectionOpen(_conn);
                using (OdbcDataAdapter dataAdapter = new OdbcDataAdapter("select * from ItemSaleLines IS,Items IT where IS.ItemID=IT.ItemID and SaleID=" + saleID, _connectionCLS.odbc))
                {
                    DataSet dsInvoiceItems = new DataSet();
                    dataAdapter.Fill(dsInvoiceItems);
                    _connectionCLS.ConnectionClose();
                    dataAdapter.Dispose();
                    return dsInvoiceItems.Tables[0];
                }
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in MYSQLDataSync class, function name getMYOBInvoiceItems [MyOB.DAL] :-" + ex.Message); 
                return null;
                //throw;
            }
        }
     /// <summary>
        /// update  order status in live database, so that next time it does not come in  queue again. 
     /// </summary>
     /// <param name="orderID">Pass order id</param>
        private void updateOrderStatusOnMySqlDatabase(string orderID)
        {
            try
            {
                _connectionCLS.MySQlConnectionOpen(_MySQlConn);

                if (_connectionCLS.mySqlConn.State == System.Data.ConnectionState.Open)
                {
                    using (MySqlCommand _mysqlCommand = new MySqlCommand("update purchase_orders set status=2 where id=" + orderID, _connectionCLS.mySqlConn))
                    {
                       _mysqlCommand.ExecuteNonQuery();
                        _connectionCLS.mySqlConn.Close();
                        _connectionCLS.mySqlConn.Dispose();
                    }
                }
              
            }
            catch (Exception ex)
            {
                _connectionCLS.mySqlConn.Close();
                _connectionCLS.mySqlConn.Dispose();
                ErrorClass.WriteError("Error in MYSQLDataSync class, function name updateOrderStatusOnMySqlDatabase [MyOB.DAL] :-" + ex.Message); 
               // throw;
            }
        }
      /// <summary>
      /// Get New invoice id from sale table
      /// </summary>
      /// <returns>Invoice Number</returns>
        private string getNewMYOBInvoiceID()
        {
            try
            {
                //InventoryAdjustmentLines
                _command = new OdbcCommand("Select s.InvoiceNumber from  Sales s order by s.SaleID desc");
                //Open myob file connection 
                _connectionCLS.ConnectionOpen(_conn);
                _command.Connection = _connectionCLS.odbc;
                OdbcDataReader _dataReader = _command.ExecuteReader();
                 InvoiceNumber = string.Empty;
                int fCount = _dataReader.FieldCount;
                while (_dataReader.Read())
                {

                    InvoiceNumber = _dataReader["InvoiceNumber"].ToString();
                    break;
                }
                //close and dispose DataReader object
                _dataReader.Close();
                _dataReader.Dispose();
                _connectionCLS.odbc.Close();
                return InvoiceNumber;
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in MYSQLDataSync class, function name getNewMYOBInvoiceID[MyOB.DAL] :-" + ex.Message);
                return null;
            }
        }
      /// <summary>
      ///check Invoice update on local database  
      /// </summary>
      /// <param name="purchase_order_number"> Pass purcahse order number</param>
      /// <returns>Get new invoce status with full detail in "DataTable" </returns>
        private DataTable checkMYOBInvoiceStatus(string purchase_order_number)
        {
            try
            {
               _connectionCLS.ConnectionOpen(_conn);
               using (OdbcDataAdapter dataAdapter = new OdbcDataAdapter("Select s.* from Sales s where (s.CustomerPONumber='" + purchase_order_number + "') order by s.SaleID desc", _connectionCLS.odbc))
               {
                   DataSet dsSales = new DataSet();
                   dataAdapter.Fill(dsSales);
                   _connectionCLS.ConnectionClose();
                   dataAdapter.Dispose();
                   return dsSales.Tables[0];
               }
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in MYSQLDataSync class, function name checkMYOBInvoiceStatus [MyOB.DAL]:-" + ex.Message);
                return null;
               // throw;
            }
        }
        private DataTable getMYOBInvoice(string InvoiceNumber)
        {
            try
            {
                _connectionCLS.ConnectionOpen(_conn);
                using (OdbcDataAdapter dataAdapter = new OdbcDataAdapter("Select s.* from Sales s where (s.InvoiceNumber='" + InvoiceNumber + "') order by s.SaleID desc", _connectionCLS.odbc))
                {
                    DataSet dsSales = new DataSet();
                    dataAdapter.Fill(dsSales);
                    _connectionCLS.ConnectionClose();
                    dataAdapter.Dispose();
                    return dsSales.Tables[0];
                }
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in MYSQLDataSync class, function name checkMYOBInvoiceStatus [MyOB.DAL]:-" + ex.Message);
                return null;
                // throw;
            }
        }
     /// <summary>
        /// read myob log file
     /// </summary>
     /// <param name="filePath">pass myob log file path</param>
     /// <returns>return log text from file</returns>
        private string readMYOBLogFile(string filePath)
        {
            try
            {
            string _meesage = string.Empty;
            //  return System.IO.File.ReadAllText(@"E:\Commit\MYOBPLOG.TXT");
            using (StreamReader streamReader = new StreamReader(filePath))
            {
                _meesage = streamReader.ReadToEnd();
                streamReader.Close();
            }

            resetMyObLogFile(filePath);
            return _meesage;
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in MYSQLDataSync class, function name checkMYOBInvoiceStatus [MyOB.DAL]:-" + ex.Message);
              return null;
            }
        

        }
      /// <summary>
        /// reset myob log file
      /// </summary>
      /// <param name="filePath">log file path </param>
        private void resetMyObLogFile(string filePath)
        {
            try
            {
                  FileStream fileStream = File.Open(filePath, FileMode.Open);
            /* 
             * Set the length of filestream to 0 and flush it to the physical file.
             *
             * Flushing the stream is important because this ensures that
             * the changes to the stream trickle down to the physical file.
             * 
             */
            fileStream.SetLength(0);
            fileStream.Close(); // This flushes the content, too.
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in MYSQLDataSync class, function name resetMyObLogFile[MyOB.DAL] :-" + ex.Message);
            }
        }

      /// <summary>
      /// Add invoice log status on live database
      /// </summary>
      /// <param name="orderNumber">Pass order number </param>
      /// <param name="message">pass log's message text</param>
        private void createLiveLog(string orderNumber,string message)
        {
            if (message == null)
            {
                return;
            }
            if (message == string.Empty)
            {
                message = "##" + orderNumber + " order not created successfully in myob database";
            }
            try
            {
                string _query = "insert into process_logs  (nursery_GLN,purchase_order_number,action_description,created) values";
                _query += "(" + _GLNNumber + ",'" + orderNumber + "','" + message + "','" + Common.mysqlDateformat(DateTime.Now.ToString()) + "')";

                _connectionCLS.MySQlConnectionOpen(_MySQlConn);

                if (_connectionCLS.mySqlConn.State == System.Data.ConnectionState.Open)
                {
                    MySqlCommand _mysqlCommand = new MySqlCommand(_query, _connectionCLS.mySqlConn);
                    _mysqlCommand.ExecuteNonQuery();

                    _connectionCLS.mySqlConn.Close();
                    _connectionCLS.mySqlConn.Dispose();
                }

            }
            catch (Exception ex)
            {
                _connectionCLS.mySqlConn.Close();
                _connectionCLS.mySqlConn.Dispose();
                ErrorClass.WriteError("Error in MYSQLDataSync class, function name createLiveLog[MyOB.DAL] :-" + ex.Message);
               // throw;
            }
        }


    }
}
