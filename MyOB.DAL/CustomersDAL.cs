﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.Interface;
using System.Data.Odbc;
using ErrorLog;
using System.Data;

namespace MyOB.DAL
{
   public class CustomersDAL
   {
       List<CustomersList> _list = new List<CustomersList>();
       ConnectionCLS _connectionCLS = new ConnectionCLS();
       MYOConnectionSetting _conn;
       String _MySQlConn;
      // OdbcCommand _command;
       string _query = string.Empty;
      /// <summary>
       /// Customers class constructor
      /// </summary>
       /// <param name="conn">Pass MyOB connection setting,for this is use MYOConnectionSetting class object</param>
       /// <param name="MySQlConn">MYSQL connection string for online portal databae </param>
       public CustomersDAL(MYOConnectionSetting conn, String MySQlConn)
       {
           _conn = conn;
           _MySQlConn = MySQlConn;
       }

       /// <summary>
       /// Get data from Customers table
       /// </summary>
       /// <returns>Customer List<ItemsList> object</returns>
       public List<CustomersList> getCustomers()
       {
           try
           {
               OdbcCommand _command = new OdbcCommand("Select * from customers");
               _connectionCLS.ConnectionOpen(_conn);
               _command.Connection = _connectionCLS.odbc;
               OdbcDataReader _dataReader = _command.ExecuteReader();
               int fCount = _dataReader.FieldCount;
               while (_dataReader.Read())
               {
                   _list.Add(new CustomersList
                   {
                       CustomerID = Convert.ToInt32(_dataReader["CustomerID"]),
                       FirstName = _dataReader["FirstName"].ToString(),
                       LastName = _dataReader["LastName"].ToString(),
                       CardIdentification = _dataReader["CardIdentification"].ToString(),
                       VolumeDiscount = Convert.ToDouble(_dataReader["VolumeDiscount"]),
                       CurrentBalance = Convert.ToDouble(_dataReader["CurrentBalance"]),
                       TotalDeposits = Convert.ToDouble(_dataReader["TotalDeposits"]),
                       CreditLimit = Convert.ToDouble(_dataReader["CreditLimit"]),
                       LastSaleDate = Convert.ToDateTime(_dataReader["LastSaleDate"])

                   });

               }
               //close and dispose DataReader object
               _dataReader.Close();
               _dataReader.Dispose();
               _connectionCLS.ConnectionClose();
               return _list;
           }
           catch (Exception ex)
           {
               ErrorClass.WriteError("Error in CustomersDAL class, function name getCustomers[MyOB.DAL] :-" + ex.Message);
               return null;
           }
       }
       /// <summary>
       /// check customer exists in myob database or not?
       /// </summary>
       /// <param name="name">customer name</param>
       /// <returns>If customer exist then "true" otherwise "false" value</returns>
       public bool checkcustomerIsExists(string name)
       {
           try
           {
            
               using (OdbcCommand _command = new OdbcCommand("Select COUNT(*)as Total from customers c where (c.LastName='" + name + "')"))
               {
                   _connectionCLS.ConnectionOpen(_conn);
                   _command.Connection = _connectionCLS.odbc;
                  if (Convert.ToInt32(_command.ExecuteScalar()) > 0)
                   {
                       _connectionCLS.ConnectionClose();
                       return true;
                   }
                   else
                   {
                       _connectionCLS.ConnectionClose();
                       return false;
                   }
                   //using (OdbcDataReader _dataReader = _command.ExecuteReader())
                   //{
                   //    int fCount = _dataReader.FieldCount;
                   //    if (_dataReader.HasRows)
                   //    {
                   //        _dataReader.Close();
                   //        _dataReader.Dispose();
                   //        _connectionCLS.ConnectionClose();
                   //        return true;
                   //    }
                   //    else
                   //    {
                   //        _dataReader.Close();
                   //       _dataReader.Dispose();
                   //        _connectionCLS.ConnectionClose();
                   //        return false;
                   //    }
                   //}
                  
                  
               }
              
               //close and dispose DataReader object
             

           }
           catch (Exception ex)
           {
               _connectionCLS.ConnectionClose();
               ErrorClass.WriteError("Error in CustomersDAL class, function name checkcustomerIsExists[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
       /// <summary>
       /// Get Max customer id
       /// </summary>
       /// <returns> Max customer id</returns>
       private Int32 getMaxID()
       {
           try
           {
               //InventoryAdjustmentLines
               Int32 CustomerID =0;
               using (OdbcCommand _command = new OdbcCommand("Select c.CustomerID from  customers c order by c.CustomerID desc"))
               {
                   //Open myob file connection 
                   _connectionCLS.ConnectionOpen(_conn);
                   _command.Connection = _connectionCLS.odbc;
                   CustomerID = Convert.ToInt32(_command.ExecuteScalar());
               }
               return CustomerID;
           }
           catch (Exception ex)
           {
               _connectionCLS.ConnectionClose();
               ErrorClass.WriteError("Error in CustomersDAL class, function name getMaxID[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
       /// <summary>
       /// Add new customer in myob database with the help "Import_Customer_Cards" table
       /// </summary>
       /// <param name="item">Import Customers List</param>
       public void AddCustomer(ImportCustomersList item)
       {
           try
           {
                item.CardID= "CUS00"+  (getMaxID()+1).ToString();
                _query = string.Empty;
               
                using (OdbcCommand _command = new OdbcCommand(_query))
                {
                    _query += "Insert into Import_Customer_Cards (CoLastName,CardStatus,Address1AddressLine1,CardID) VALUES (?,?,?,?) ";

                    _command.CommandType = CommandType.Text;
                    _command.CommandText = _query;
                    _command.Parameters.AddWithValue("@CoLastName", item.CoLastName);
                    _command.Parameters.AddWithValue("@CardStatus", "N");
                    _command.Parameters.AddWithValue("@Address1AddressLine1",item.Address1AddressLine1);
                    _command.Parameters.AddWithValue("@CardID", item.CardID);
                    
                    _connectionCLS.ConnectionOpen(_conn);
                    _command.Connection = _connectionCLS.odbc;
                    _command.ExecuteNonQuery();
                    _command.CommandText = "END TRANSACTION";
                    _command.ExecuteNonQuery();

                    _command.Connection.Close();
                    _command.Connection.Dispose();
                    _connectionCLS.ConnectionClose();
                }

           }
           catch (Exception ex)
           {
               using (OdbcCommand _command = new OdbcCommand())
               {
                   _command.Connection = _connectionCLS.odbc;
                   _connectionCLS.ConnectionOpen(_conn);
                   _command.CommandText = "END TRANSACTION";
                   _command.ExecuteNonQuery();
               }
               _connectionCLS.ConnectionClose();
               ErrorClass.WriteError("Error in CustomersDAL class, function name AddCustomer[MyOB.DAL] :-" + ex.Message);
               throw;
           }
       }
    }
}
