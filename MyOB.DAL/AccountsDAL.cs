﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.Interface;
using System.Data;
using System.Data.Odbc;

namespace MyOB.DAL
{
    public class AccountsDAL
    {
        MYOConnectionSetting _conn;
        ConnectionCLS _objConnection = new ConnectionCLS();
        // Declare DataColumn and DataRow variables.
        DataRow row;
        DataView view;
   
        public AccountsDAL(MYOConnectionSetting conn)
        {  
            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            _conn = conn;
        }
        #region Get Account setting's values functions 
        public DataTable getPurchasesAccounts()
        {
            try
            {
                _objConnection.ConnectionOpen(_conn);
                using (OdbcDataAdapter dataAdapter = new OdbcDataAdapter("select s.AccountNumber+' ['+s.AccountName+']' as Name,s.AccountNumber from accounts s where (s.AccountClassificationID='COS' and s.IsInactive='N')", _objConnection.odbc))
                {
                    DataSet dsAccounts = new DataSet();
                    dataAdapter.Fill(dsAccounts);
                    dataAdapter.Dispose();
                    _objConnection.ConnectionClose();

                    row = dsAccounts.Tables[0].NewRow();
                    row["Name"] = "Select";
                    row["AccountNumber"] = "0";
                    dsAccounts.Tables[0].Rows.InsertAt(row, 0);
                    return dsAccounts.Tables[0];
                }
                
            }
            catch (Exception ex)
            {
                
             return null;
            }
        }
        public DataTable getSaleAccounts()
        {
            try
            {
                _objConnection.ConnectionOpen(_conn);
                using (OdbcDataAdapter dataAdapter = new OdbcDataAdapter("select s.AccountNumber+' ['+s.AccountName+']' as Name,s.AccountNumber from accounts s where (s.AccountClassificationID='I' and s.IsInactive='N')", _objConnection.odbc))
                {
                    DataSet dsAccounts = new DataSet();
                    dataAdapter.Fill(dsAccounts);
                    dataAdapter.Dispose();
                    _objConnection.ConnectionClose();
                    row = dsAccounts.Tables[0].NewRow();
                    row["Name"] = "Select";
                    row["AccountNumber"] = "0";
                    dsAccounts.Tables[0].Rows.InsertAt(row, 0);
                    return dsAccounts.Tables[0];
                }

            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public DataTable getTradeDebtorsAccounts()
        {
            try
            {
                _objConnection.ConnectionOpen(_conn);
                using (OdbcDataAdapter dataAdapter = new OdbcDataAdapter("select s.AccountNumber+' ['+s.AccountName+']' as Name,s.AccountNumber from accounts s where (s.AccountClassificationID='A' and s.IsInactive='N')", _objConnection.odbc))
                {
                    DataSet dsAccounts = new DataSet();
                    dataAdapter.Fill(dsAccounts);
                    dataAdapter.Dispose();
                    _objConnection.ConnectionClose();
                    row = dsAccounts.Tables[0].NewRow();
                    row["Name"] = "Select";
                    row["AccountNumber"] = "0";
                    dsAccounts.Tables[0].Rows.InsertAt(row, 0);
                    return dsAccounts.Tables[0];
                }

            }
            catch (Exception ex)
            {

                return null;
            }
        }
        #endregion

        private bool checkPurcasesAccount(string AccountNumber)
        {
            try
            {
                 using (OdbcCommand command = new OdbcCommand())
                    {
                        command.CommandText = "select AccountID from accounts s where (s.IsInactive='N' and s.AccountClassificationID='COS' and s.AccountNumber='" + AccountNumber + "')";
                        _objConnection.ConnectionOpen(_conn);
                        command.Connection = _objConnection.odbc;
                        var _ids = command.ExecuteScalar();
                        if(_ids!=null)
                        {
                            _objConnection.ConnectionClose();
                            return true;
                        }
                        else
                        {
                            _objConnection.ConnectionClose();
                            return false;
                        }
                    }
            }
            catch (Exception ex)
            {
                
               _objConnection.ConnectionClose();
                            return false;
            }
        }
    }
}
