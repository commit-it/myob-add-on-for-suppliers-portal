﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.Interface;
using System.Data.Odbc;
using System.Data;
using ErrorLog;
namespace MyOB.DAL
{
   public class SuppliersDAL
    {
      OdbcCommand _command;
      ConnectionCLS _connectionCLS = new ConnectionCLS();
      MYOConnectionSetting _conn;
      public SuppliersDAL(MYOConnectionSetting conn)
       {
           _conn = conn;
           
       }
       /// <summary>
       /// Add new supplier
       /// </summary>
      /// <param name="_supplier">Pass SuppliersList class object</param>
      public void AddSupplier(SuppliersList _supplier)
      {
          try

          {
              //call getMaxID() function for geting Max ID from Suppliers table
              _supplier.CardID = "SUPP00" + (getMaxID() + 1).ToString();

              //open myob file connection
              _connectionCLS.ConnectionOpen(_conn);
              OdbcCommand _command = new OdbcCommand();
              string _query = string.Empty;
              _query += "(CoLastName,CardStatus,CardID) VALUES ";
              _query += "('" + _supplier.Name + "','N','"+_supplier.CardID+"')";
              _command.CommandText = "INSERT INTO Import_Supplier_Cards "+_query;
              // "INSERT INTO Import_NonConsolidated_TaxCodes(TaxCode,Description,TaxType,Rate) VALUES('TGT','BAT',2,10)";
              _command.Connection = _connectionCLS.odbc;
              _command.ExecuteNonQuery();
              _command.CommandText = "END TRANSACTION";
              _command.ExecuteNonQuery();
           
              _command.Connection.Close();
              _command.Connection.Dispose();
              
          }
          catch (Exception ex)
          {
              ErrorClass.WriteError("Error in SuppliersDAL, function name AddSupplier [MyOB.DAL]:-" + ex.Message);
              throw;
          }
      }
      
       /// <summary>
       /// Check supplier in supplier table
       /// </summary>
       /// <param name="name">Pass last name</param>
       /// <returns>True/false</returns>
      public bool checkSupplierIsExists(String name)
      {
          try
          {
              //select * from Cards
              _connectionCLS.ConnectionOpen(_conn);
              OdbcDataAdapter da = new OdbcDataAdapter("select Card.CardRecordID from Cards Card where(Card.CardTypeID='S' and Card.LastName='" + name + "')", _connectionCLS.odbc);
              DataSet ds = new DataSet();
              da.Fill(ds);
              
           
              if (ds != null && ds.Tables[0].Rows.Count>0)
              {
                  ds.Dispose();
                  _connectionCLS.ConnectionClose();
                  return true;
              }
              else {
                  ds.Dispose();
                  _connectionCLS.ConnectionClose();
                  return false;
              }
          }
          catch (Exception ex)
          {
              ErrorClass.WriteError("Error in SuppliersDAL, function name checkSupplierIsExists [MyOB.DAL]:-" + ex.Message);
              _connectionCLS.ConnectionClose();
              return false;
          }
      }
  /// <summary>
      /// function for geting Supplier's MaxID from Suppliers table
  /// </summary>
      /// <returns>maximum SupplierID</returns>
      private Int32 getMaxID()
      {
          try
          {
              //InventoryAdjustmentLines
              _command = new OdbcCommand("Select s.SupplierID from  Suppliers s order by s.SupplierID desc");
              //Open myob file connection 
              _connectionCLS.ConnectionOpen(_conn);
              _command.Connection = _connectionCLS.odbc;
              OdbcDataReader _dataReader = _command.ExecuteReader();
              Int32 CustomerID = 0;
              int fCount = _dataReader.FieldCount;
              while (_dataReader.Read())
              {

                  CustomerID = Convert.ToInt32(_dataReader["SupplierID"]);
                  break;
              }
              //close and dispose DataReader object
              _dataReader.Close();
              _dataReader.Dispose();
              _connectionCLS.ConnectionClose();
              return CustomerID;
          }
          catch (Exception ex)
          {
              _connectionCLS.ConnectionClose();
              ErrorClass.WriteError("Error in SuppliersDAL, function name getMaxID [MyOB.DAL]:-" + ex.Message);
              throw;
          }
      }
    }
}
