﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyOB.Interface
{
  public class SuppliersList
    {
      /// <summary>
      /// SupplierID
      /// </summary>
        public int SupplierID { get; set; }
      /// <summary>
      /// The CardRecordID (Cards) of the record within the Cardstable containing information about the same card.
      /// </summary>
        public int CardRecordID { get; set; }
      /// <summary>
      /// User defined unique CardID.
      /// </summary>
        public string CardIdentification { get; set; }
      /// <summary>
      /// If the card is a company: company name.If the card is an individual: last name followed by a comma,a space and the first name.
      /// </summary>
        public string Name { get; set; }
      /// <summary>
      /// The company name if card is a company, or last name ifcard is an individual.
      /// </summary>
        public string LastName { get; set; }
      /// <summary>
      /// Blank if the card is a company, or first name if card is anindividual.
      /// </summary>
        public string  FirstName { get; set; }
      /// <summary>
      /// Whether the card record represents an individual.Company='N',Individual='Y'. 
      /// </summary>
        public char  IsIndividual { get; set; }
      /// <summary>
      /// Whether the company or indiviual represented by the card record is inactive. Active='N',Inactive='Y'
      /// </summary>
        public char IsInactive { get; set; }
      /// <summary>
      /// The CurrencyID (Currency) of the record containing currency information for this card.
      /// </summary>
        public string CurrencyID { get; set; }
      /// <summary>
      /// Name of bitmap attached to this card.
      /// The picture will be stored within graphics sub-folder
      /// </summary>
        public string Picture { get; set; }
      /// <summary>
      /// Notes for this card
      /// </summary>
        public string Notes { get; set; }
      /// <summary>
      /// Entry in this field is conditional. A value must exist within this field if
      ///no value exists within the CoLastName and RecordID fields
      /// </summary>
        public string  CardID { get; set; }
    }
}
