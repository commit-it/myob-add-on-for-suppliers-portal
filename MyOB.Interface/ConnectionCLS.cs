﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using ErrorLog;

namespace MyOB.Interface
{
    public  class ConnectionCLS
    {
        /// <summary>
        /// odbc connection object
        /// </summary>
        public  OdbcConnection odbc;
        /// <summary>
        /// Mysql connection for online Prtal 
        /// </summary>
        public  MySqlConnection mySqlConn;

        ///// <summary>
        /////Pass .myo file path
        ///// </summary>
        ///// <param name="DataBaseFilePath">Myob Database file path</param>
        //public  void ConnectionOpen(string myobDataBaseFilePath)
        //{
        //    try
        //    {
        //        odbc = new OdbcConnection(@"Driver={MYOAU1001}; TYPE=MYOB; UID=Administrator; PWD=;DATABASE=" + myobDataBaseFilePath + @"; HOST_EXE_PATH=C:\Users\admin\AppData\Local\Programs\MYOB\MYOB AccountRight 2015.1\Upgrader\AU\MYOBP.exe;NETWORK_PROTOCOL=NONET; DRIVER_COMPLETION=DRIVER_NOPROMPT;");
        //    if (odbc.State != System.Data.ConnectionState.Open)
        //    {
        //        odbc.Open();
        //    }
        //    }
        //    catch (Exception)
        //    {
                
        //    }
        //}

      /// <summary>
        /// Pass .myo file login detail such as uid,password and file path
      /// </summary>
        /// <param name="conn">MYOConnectionSetting</param>
        /// <param name="message">Out string parameter. </param>
        /// <returns>Connection status</returns>
        public bool ConnectionOpen(MYOConnectionSetting conn, out string message)
        {
            try
            {
                //String _test=@"Driver={MYOAU1001}; TYPE=MYOB; UID=Administrator; PWD=Welcome01;DATABASE=E:\Commit\Scotts Tubes Myles Test.myo;ACCESS_TYPE=READ_WRITE;HOST_EXE_PATH=C:\Premier19\Myobp.exe;NETWORK_PROTOCOL=NONET; DRIVER_COMPLETION=DRIVER_NOPROMPT;KEY='546233030400A014267D3B280107358F387C2CE958B86E1EE77C7A8C980DC4DB6FC58947905BBE6ACE1F0E03527E5FE07B7DB950CAFD7DAB3D6611978C50B85B';SQL_ATTR_AUTOCOMMIT=1;";
                odbc = new OdbcConnection(@"Driver=MYOAU1001;DATABASE=" + conn.MYOBFilePath.Trim() + ";TYPE=MYOB;HOST_EXE_PATH=" + conn.MYOBHostExePath + ";UID=" + conn.UID + ";PWD=" + conn.PWD + ";ACCESS_TYPE=READ_WRITE;DRIVER_COMPLETION=DRIVER_NOPROMPT;NETWORK_PROTOCOL=TCPIP;KEY=" + conn.MYOBKey.Trim() + ";SQL_ATTR_AUTOCOMMIT=1;");
                //odbc = new OdbcConnection(@"Driver=MYOAU1001;TYPE=MYOB; UID=" + conn.UID + "; PWD=" + conn.PWD + ";DATABASE=" + conn.MYOBFilePath + @";ACCESS_TYPE=READ_WRITE;HOST_EXE_PATH=" + conn.MYOBHostExePath + ";NETWORK_PROTOCOL=TCPIP; DRIVER_COMPLETION=DRIVER_NOPROMPT;KEY='" + conn.MYOBKey + "';SQL_ATTR_AUTOCOMMIT=1;");
               // odbc = new OdbcConnection(_test);
                if (odbc.State != System.Data.ConnectionState.Connecting)
                {
                    if (odbc.State != System.Data.ConnectionState.Open)
                    {
                        odbc.Open();
                        message = "MyOb database connected successfully";
                        return true;
                    }
                    else
                    {
                        message = "MyOb database alredy connected";
                        return false;
                    }
                }
                else
                {
                    message = "try to connecting MyOb database";
                    return false;
                }
            }
            catch (Exception ex)
            {
                message =ex.Message ;
                return false;
              //  MessageBox.Show("MYOB login details are incorrect.");
                //throw;
                
            }
        }

         /// <summary>
        /// Pass .myo file login detail such as uid,password and file path
         /// </summary>
        /// <param name="conn">MYOConnectionSetting</param>
        /// <returns>Connection status</returns>
        public bool ConnectionOpen(MYOConnectionSetting conn)
        {
            try
            {
                odbc = new OdbcConnection(@"Driver=MYOAU1001;DATABASE=" + conn.MYOBFilePath.Trim() + ";TYPE=MYOB;HOST_EXE_PATH=" + conn.MYOBHostExePath + ";UID=" + conn.UID + ";PWD=" + conn.PWD + ";ACCESS_TYPE=READ_WRITE;DRIVER_COMPLETION=DRIVER_NOPROMPT;NETWORK_PROTOCOL=TCPIP;KEY=" + conn.MYOBKey.Trim() + ";SQL_ATTR_AUTOCOMMIT=1;");
                //odbc = new OdbcConnection(@"Driver={MYOAU1001}; TYPE=MYOB; UID=" + conn.UID + "; PWD=" + conn.PWD + ";DATABASE=" + conn.MYOBFilePath + @"; ACCESS_TYPE=READ_WRITE;HOST_EXE_PATH=C:\Premier19\MYOBP.exe;NETWORK_PROTOCOL=NONET; DRIVER_COMPLETION=DRIVER_PROMPT;KEY='0FB0C279BBF509F4B8ED9ED8FCD33EC33E8DFA5EEC25CA93569B1D763043FDE0845C00726510FE3E7BCD5AAAC25FC3546E255D95CCBBACCAA157F44F9DE2B884';SQL_ATTR_AUTOCOMMIT=1;");
                if (odbc.State != System.Data.ConnectionState.Connecting)
                {
                    if (odbc.State != System.Data.ConnectionState.Open)
                    {
                        odbc.Open();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in ConnectionCLS.cs, function name ConnectionOpen [MyOB.Interface]:-" + ex.Message);
                return false;
                //  MessageBox.Show("MYOB login details are incorrect.");
                //throw;

            }
        }
        /// <summary>
        /// Pass Mysql connection string 
        /// </summary>
        /// <param name="connectionString">Connection status</param>
        public bool MySQlConnectionOpen(string connectionString)
        {
            try
            {
                mySqlConn = new MySqlConnection(connectionString);
                if (mySqlConn.State != ConnectionState.Open)
                {
                    mySqlConn.Open();
                    
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in ConnectionCLS.cs, function name MySQlConnectionOpen [MyOB.Interface]:-" + ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Function for opne MySQl Connection
        /// </summary>
        /// <param name="connectionString">MySQL connection string detail </param>
        /// <param name="message">out parameter for message,it will return connectoin status message </param>
        /// <returns>connectoin status true/false</returns>
        public bool MySQlConnectionOpen(string connectionString, out string message)
        {
            try
            {
                mySqlConn = new MySqlConnection(connectionString);
                if (mySqlConn.State != ConnectionState.Open)
                {
                    mySqlConn.Open();
                    message = "Web portal database connected successfully";
                }
                else {
                    message = "Web portal database alredy connected";
                }
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                ErrorClass.WriteError("Error in ConnectionCLS.cs, function name MySQlConnectionOpen, out string message [MyOB.Interface]:-" + ex.Message);
                return false;
            }
        }
        /// <summary>
        ///close mysql connection
        /// </summary>
        public void MySQlConnectionClose()
        {
            try
            {
                if (mySqlConn != null)
                {
                    if (mySqlConn.State == ConnectionState.Open)
                    {
                        mySqlConn.Close();
                        mySqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in ConnectionCLS.cs, function name MySQlConnectionClose [MyOB.Interface]:-" + ex.Message);
            }
        }
        /// <summary>
        /// close odbc connection
        /// </summary>
        public void ConnectionClose()
        {
            try
            {
                if (odbc.State == ConnectionState.Open)
                {
                    odbc.Close();
                    odbc.Dispose();
                }
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error in ConnectionCLS.cs, function name ConnectionClose [MyOB.Interface]:-" + ex.Message);
                
            }
        }
    }

    /// <summary>
    /// Connection Setting class, pass the connection values for .myo file.
    /// </summary>
    public class MYOConnectionSetting
    {
        /// <summary>
        /// User ID or User Name
        /// </summary>
        public string UID { get; set; }
        /// <summary>
        /// Password 
        /// </summary>
        public string PWD { get; set; }
        /// <summary>
        /// .myo file path 
        /// </summary>
        public string MYOBFilePath { get; set; }
        /// <summary>
        /// path and name of the AccountRight application For instance "C:\Premier19\MYOBP.exe"
        /// </summary>
        public string MYOBHostExePath { get; set; }
        /// <summary>
        /// Pass key value
        /// </summary>
        public string MYOBKey { get; set; }

    }
}
