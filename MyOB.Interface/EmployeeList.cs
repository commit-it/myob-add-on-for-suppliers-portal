﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyOB.Interface
{
    /// <summary>
    ///Class for import new Employee
    /// </summary>
   public class ImportEmployeeList
    {
       /// <summary>
        /// Employee last name
       /// </summary>
        public string  CoLastName { get; set; }
       /// <summary>
        /// Employee first name
       /// </summary>
        public string FirstName { get; set; }
       /// <summary>
        /// Employee address
       /// </summary>
        public string Address { get; set; }
       /// <summary>
        /// Employee card id
       /// </summary>
        public string CardID { get; set; }
    }
}
