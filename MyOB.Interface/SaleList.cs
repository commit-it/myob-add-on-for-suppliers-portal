﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyOB.Interface
{
    /// <summary>
    /// Sales table class 
    /// </summary>
    public class SaleList
    {
     
        public int SaleID { get; set; }
        public string InvoiceNumber { get; set; }
        public string CustomerPONumber { get; set; }
        public string IsHistorical { get; set; }
        public string ShipToAddres { get; set; }
        public int BackorderSaleID { get; set; }
        public DateTime InvoiceDate { get; set; } 

        public string  StatusID { get; set; }
        public string InvoiceStatusID { get; set; }

        public double TotalLines { get; set; }
        public double TotalTax { get; set; }
        public double TotalPaid { get; set; }
        public double TotalDeposits { get; set; }
        public double TotalCredits { get; set; }
        public double TotalDiscount { get; set; }
        public double OutStandingBalance { get; set; }
       public string Comment { get; set; }

        public DateTime PromisedDate { get; set; }
        public string IsTaxInclusive { get; set; }
        public string IsAutoRecorded { get; set; }
        public int DaysTillPaid { get; set; }
    }
    
    public class ImportSaleInvoice{
        /// <summary>
        ///A value must exist within this field if no value exists within the CardID and RecordID fields
        /// </summary>
        public string CoLastName { get; set; }
        /// <summary>
        /// If a value exists in this field, the Designation of the Card is
        ///Individual. When the card is Individual, the format of the
        ///CoLastName field is 30xAN.
        ///If no value exists in this field, the Designation of the Card is
        ///Company. When the card is Company, the format of the
        ///CoLastName field is 50xAN.
        /// </summary>
        public string FirstName { get; set; }
        public string AddressLine1 { get; set; }
        /// <summary>
        /// Date format is controlled within the Regional Settings of the
        ///operating system
        /// </summary>
        public string SaleDate { get; set; }
        /// <summary>
        /// The purchase order number provided by the customer.
        /// </summary>
        public string  CustomersNumber { get; set; }
        public string  ShipVia { get; set; }
        public string  ItemNumber { get; set; }
      
        /// <summary>
        /// P= To be printed
        /// E = To be emailed
        ///B = To be printed and emailed
        ///A = Already printed or sent
        /// </summary>
        public string DeliveryStatus { get; set; }
        /// <summary>
        /// Negative values are indicated as such by way of a leading negative sign.
        /// </summary>
        public Double Quantity { get; set; }
        public string Description { get; set; }
        public Double Price { get; set; }
        public string Comment { get; set; }

        public string SalespersonFirstName { get; set; }

        public string ShippingDate { get; set; }

    }
}
