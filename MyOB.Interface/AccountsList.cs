﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyOB.Interface
{
    /// <summary>
    /// Account class for account's setting values 
    /// </summary>
   public class AccountsList
    {
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
    }
}
