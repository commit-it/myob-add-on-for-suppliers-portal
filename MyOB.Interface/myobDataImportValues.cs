﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyOB.Interface
{
   public class myobDataImportValues
    {
        public string localLOGfile { get; set; }
        public string glnNumber { get; set; }
        public string tax { get; set; }
        public string salesPerson { get; set; }
        public string shipVia { get; set; }
        public string address { get; set; }
        public int IncomeAccountforTrackingSales { get; set; }
        public int AssestAccountItemInvento { get; set; }
        public int CostOfSaleAccount { get; set; }
        public string MyoHostExePath { get; set; }

    }
}
