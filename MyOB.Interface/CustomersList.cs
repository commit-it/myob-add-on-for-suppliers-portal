﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyOB.Interface
{
    /// <summary>
    /// MYOB Customer class for customer detail
    /// </summary>
    public class CustomersList
    {
        public int CustomerID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string CardIdentification { get; set; }
        public string  Notes{ get; set; }
        public string OnHold { get; set; }

        public double VolumeDiscount { get; set; }
        public double CurrentBalance { get; set; } 
        public double TotalDeposits { get; set; }
        public double CreditLimit { get; set; }

        public DateTime CustomerSince { get; set; }
        public DateTime LastSaleDate { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public int TotalReceivableDays { get; set; }
        public int TotalPaidInvoices { get; set; }
        public double HighestInvoiceAmount { get; set; }
        public double HighestReceivableAmount { get; set; }
        

    }
    /// <summary>
    /// Class for add new Customer "ImportCustomersList"
    /// </summary>
    public class ImportCustomersList
    {
        /// <summary>
        /// Entry in this field is conditional. A value must exist within this field
        ///if no value exists within the CardID and RecordID fields.
        /// </summary>
        public string CoLastName { get; set; }
        /// <summary>
        /// If a value exists in this field, the Designation of the Card is
        ///Individual. When the card is Individual, the format of the
        ///CoLastName field is 30xAN.
        ///If no value exists in this field, the Designation of the Card is
        ///Company. When the card is Company, the format of the
        ///CoLastName field is 50xAN.
        /// </summary> 
        public string FirstName { get; set; }

        public string Address1AddressLine1 { get; set; }
        /// <summary>
        /// Entry in this field is conditional. A value must exist within this field
        ///if no value exists within the CoLastName and RecordID fields.
        /// </summary>
        public string CardID { get; set; }
    }
}
