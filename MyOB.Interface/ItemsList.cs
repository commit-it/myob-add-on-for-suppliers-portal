﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyOB.Interface
{ 
    /// <summary>
    /// Import Item class for import data into the Item table
    /// </summary>
 public  class ImportItemsList
    {
     /// <summary>
     /// Item id 
     /// </summary>
        public int ItemID { get; set; }
        /// <summary>
        /// Item name
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// Entry in this field mandatory 
        /// </summary>
        public string ItemNumber { get; set; }
     /// <summary>
     /// A value in this field indicates that the item is bought.
     /// </summary>
        public string  Buy { get; set; }
     /// <summary>
     /// A value of this field indicates that the item is sold
     /// </summary>
        public string Sell { get; set; }
     /// <summary>
     /// A value of this field indicates that the item is Inventoired
     /// </summary>
        public string Iventroy { get; set; }
     /// <summary>
     /// Do not include the account separator. 
     /// </summary>
        public int AssetAccount { get; set; }
     /// <summary>
     /// Do not include the account separator.
     /// </summary>
        public int IncomeAccount { get; set; }
     /// <summary>
     /// Do not include the account separator.
     /// </summary>
        public int ExpenseAccount { get; set; }
     /// <summary>
     /// File name of the linked picture(bmp,jpg,tif,gif,or png).The file must be stored in the "Graphics" folder, which located within AccountRight's installation folder.
     /// </summary>
        public string ItemPicture { get; set; }
     /// <summary>
     /// Item detail
     /// </summary>
        public string Description { get; set; }
        public string CustomList1 { get; set; }
        public string CustomList2 { get; set; }
        public string CustomList3 { get; set; }

        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string CustomField3 { get; set; }
     /// <summary>
     /// Car number can be up to 50 digits in length.
     /// </summary>
        public string Primary { get; set; }
     /// <summary>
     /// Pass supplier item number such as "DV100"
     /// </summary>
        public string SupplierItemNumber { get; set; }//DV100
        public string TaxCodeWhenBought { get; set; }
        public string BuyUnitMeasure { get; set; }
        public int    NumberItemsBuyUnit { get; set; }
        public double ReorderQuantity { get; set; }
        public double MinimumLevel { get; set; } 
        public decimal SellingPrice { get; set; }
        public string SellUnitMeasure { get; set; }
     /// <summary>
     /// 'Y' for Active
     /// 'N' for InActive 
     /// </summary>
        public string InActiveItem { get; set; }
     /// <summary>
     /// 
     /// </summary>
        public decimal StandardCost { get; set; }

       
    }
 /// <summary>
 /// class for item list from Item table
 /// </summary>
 public class ItemsList:ImportItemsList {
     /// <summary>
     /// Item Quantity On Hand
     /// </summary>
     public double QuantityOnHand { get; set; }
     /// <summary>
     /// Item value on hand
     /// </summary>
     public double ValueOnHand { get; set; }
     /// <summary>
     /// Item positive average cost
     /// </summary>
     public double PositiveAverageCost { get; set; }
     /// <summary>
     /// Item last unit price 
     /// </summary>
     public double LastUnitPrice { get; set; }
     /// <summary>
     /// Item sell on order or not
     /// </summary>
     public double SellOnOrder { get; set; }
     /// <summary>
     /// Item base selling price
     /// </summary>
     public double BaseSellingPrice { get; set; }
     /// <summary>
     /// Item Tax Inclusive Last Purchase Price
     /// </summary>
     public double TaxInclusiveLastPurchasePrice { get; set; }
     /// <summary>
     /// Item Tax Inclusive Standard Cost
     /// </summary>
     public double TaxInclusiveStandardCost { get; set; }
     /// <summary>
     /// Item Tax Exclusive Standard Cost
     /// </summary>
     public double TaxExclusiveStandardCost { get; set; }
     /// <summary>
     /// Item memo
     /// </summary>
     public string  Memo { get; set; }
     /// <summary>
     /// Item quantity 
     /// </summary>
     public Int32 Quantity { get; set; }
     /// <summary>
     /// Item unit cost
     /// </summary>
     public double UnitCost { get; set; }
     /// <summary>
     /// Item amount 
     /// </summary>
     public double Amount { get; set; }
     /// <summary>
     /// Bar code
     /// </summary>
     public string Barcode { get; set; }
     public string TaxCodeWhenBought { get; set; }
 }

}
