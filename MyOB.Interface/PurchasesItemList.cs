﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;

namespace MyOB.Interface
{
   public class PurchasesItemList
    {
       /// <summary>
       /// A value must exist within this field of no value exist within ths CardID and RecordID fields.
       /// </summary>
        public string CoLastName { get; set; }
       /// <summary>
       /// If a value exist in this field, the designation of the Card is Individual.When the card is Individual, the format of the CoLastName field is 30xAN 
       /// </summary>
        public string FirstName { get; set; }
       /// <summary>
       /// Pass Address value
       /// </summary>
        public string AddressLine1 { get; set; }
       /// <summary>
       /// A value in this field indicates that sale Invoice is Tax Inclusive
       /// </summary>
        public string Inclusive { get; set; }

        public string PurchaseNumber { get; set; }
       /// <summary>
       /// Data format is controlled within the Regional Setting of the oprating system.
       /// </summary>
        public string PurchaseDate { get; set; }
       /// <summary>
       /// the invoice number provoded by supplier. 
       /// </summary>
        public string  SuppliersNumber { get; set; }
        public string ShipVia { get; set; }
       /// <summary>
       /// Pass any one value from bellow list
       /// P=To be printed 
       /// E=To be emailed
       /// B=To be emailed and printed
       /// A=Already printed or sent
       /// </summary>
        public string DeliveryStatus { get; set; }
       /// <summary>
       /// Entery in this field is mandatory.
       /// </summary>
        public string ItemNumber { get; set; }
       /// <summary>
       /// Number of Items purchased in bill
       /// Negative value are indicated as such by way of leading negative sign.
       /// </summary>
        public Double Quantity { get; set; }
       /// <summary>
       /// Item detail
       /// </summary>
        public string Description { get; set; }
        public Double ExTaxPrice { get; set; }
        public Double IncTaxPrice { get; set; }
        public Double Discount { get; set; }
        public Double ExTaxToatl { get; set; }
        public Double IncTaxTotal { get; set; }
        public string Job { get; set; }
        public string Comment { get; set; }
        public string Memo { get; set; }
        public string ShippingDate { get; set; }
        public string TaxCode { get; set; }

    }
}
