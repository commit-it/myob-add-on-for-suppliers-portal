﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Windows.Forms;

namespace ErrorLog
{
   public static class TrackInvoiceLog
    {
        /// <summary>
        /// With the help of WriteInvoiceLog function you can track your inserted Invoices' logs
        /// </summary>
       /// <param name="sData">Pass log value</param>
       public static void WriteInvoiceLog(string sData)
       {
           
           try
           {
               string path = Path.GetPathRoot(Environment.SystemDirectory) + "MyOBLogs";

               if (!Directory.Exists(path))
                   Directory.CreateDirectory(path);

               string filePath = path+ "\\Invoice_Log" + DateTime.Today.ToString("dd-MM-yy") + ".csv";
               if (!System.IO.File.Exists(filePath))
               {
                   System.IO.File.Create(filePath).Close();

                   using (StreamWriter w = System.IO.File.AppendText(filePath))
                   {
                       w.WriteLine("purchase_order_number,invoice_number,datetime");
                       w.Flush();
                       w.Close();
                   }

               }
               using (StreamWriter w = System.IO.File.AppendText(filePath))
               {
                   w.WriteLine(sData);
                   w.Flush();
                   w.Close();
               }
           }
           catch (Exception ex)
           {
               MessageBox.Show(ex.Message);
           }
       }
    }
}
