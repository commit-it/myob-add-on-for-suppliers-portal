﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Windows.Forms;

namespace ErrorLog
{ 
    public static class ErrorClass
    {
        /// <summary>
        /// With the help of WriteError function you can track your application logs
        /// </summary>
        /// <param name="sErrMsg">Pass error text </param>
        public static void WriteError( string sErrMsg)
        {
            // how to use this function ErrorLog.ErrorClass.WriteError( "Hello I am error");
            try
            {
                string path = Path.GetPathRoot(Environment.SystemDirectory) + "MyOBLogs";

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string filePath = path +"\\error_log_" + DateTime.Today.ToString("dd-MM-yy") + ".txt";


                if (!System.IO.File.Exists(filePath))
                {
                    System.IO.File.Create(filePath).Close();
                }
                using (StreamWriter w = System.IO.File.AppendText(filePath))
                {
                    w.WriteLine("Log Date and Time : {0}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    w.WriteLine(sErrMsg);
                    w.WriteLine("__________________________" + Environment.NewLine);
                    w.Flush();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                    w.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
  

    }
}
