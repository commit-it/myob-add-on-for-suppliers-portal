﻿namespace MyOB.Sync
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.timerCheckData = new System.Windows.Forms.Timer(this.components);
            this.notifyIconMyOB = new System.Windows.Forms.NotifyIcon(this.components);
            this.panelMain = new System.Windows.Forms.Panel();
            this.panelHead = new System.Windows.Forms.Panel();
            this.btnMysqlServerSettings = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnCloseWindow = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDatabaseStatus = new System.Windows.Forms.Label();
            this.imgDatabaseLive = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataSyncProgressBar = new System.Windows.Forms.ProgressBar();
            this.panelMain.SuspendLayout();
            this.panelHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDatabaseLive)).BeginInit();
            this.SuspendLayout();
            // 
            // timerCheckData
            // 
            this.timerCheckData.Interval = 30000;
            this.timerCheckData.Tick += new System.EventHandler(this.timerCheckData_Tick);
            // 
            // notifyIconMyOB
            // 
            this.notifyIconMyOB.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIconMyOB.Icon")));
            this.notifyIconMyOB.Text = "Suppliers Portal MyOB Sync";
            this.notifyIconMyOB.Visible = true;
            this.notifyIconMyOB.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // panelMain
            // 
            this.panelMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMain.Controls.Add(this.panelHead);
            this.panelMain.Controls.Add(this.txtLog);
            this.panelMain.Controls.Add(this.label2);
            this.panelMain.Controls.Add(this.lblDatabaseStatus);
            this.panelMain.Controls.Add(this.imgDatabaseLive);
            this.panelMain.Controls.Add(this.label1);
            this.panelMain.Controls.Add(this.dataSyncProgressBar);
            this.panelMain.Location = new System.Drawing.Point(-2, 1);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(582, 356);
            this.panelMain.TabIndex = 0;
            // 
            // panelHead
            // 
            this.panelHead.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelHead.Controls.Add(this.btnMysqlServerSettings);
            this.panelHead.Controls.Add(this.btnSetting);
            this.panelHead.Controls.Add(this.btnCloseWindow);
            this.panelHead.Location = new System.Drawing.Point(-1, 1);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(582, 30);
            this.panelHead.TabIndex = 16;
            // 
            // btnMysqlServerSettings
            // 
            this.btnMysqlServerSettings.Location = new System.Drawing.Point(211, 4);
            this.btnMysqlServerSettings.Name = "btnMysqlServerSettings";
            this.btnMysqlServerSettings.Size = new System.Drawing.Size(154, 23);
            this.btnMysqlServerSettings.TabIndex = 16;
            this.btnMysqlServerSettings.Text = "Web portal database settings";
            this.btnMysqlServerSettings.UseVisualStyleBackColor = true;
            this.btnMysqlServerSettings.Click += new System.EventHandler(this.btnMysqlServerSettings_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.Location = new System.Drawing.Point(3, 3);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(154, 23);
            this.btnSetting.TabIndex = 10;
            this.btnSetting.Text = "MyOb file settings";
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnCloseWindow
            // 
            this.btnCloseWindow.BackColor = System.Drawing.Color.Red;
            this.btnCloseWindow.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnCloseWindow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.btnCloseWindow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btnCloseWindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseWindow.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCloseWindow.Location = new System.Drawing.Point(541, 3);
            this.btnCloseWindow.Name = "btnCloseWindow";
            this.btnCloseWindow.Size = new System.Drawing.Size(32, 23);
            this.btnCloseWindow.TabIndex = 15;
            this.btnCloseWindow.Text = "X";
            this.btnCloseWindow.UseVisualStyleBackColor = false;
            this.btnCloseWindow.Visible = false;
            this.btnCloseWindow.Click += new System.EventHandler(this.btnCloseWindow_Click);
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtLog.ForeColor = System.Drawing.SystemColors.Window;
            this.txtLog.Location = new System.Drawing.Point(180, 37);
            this.txtLog.Margin = new System.Windows.Forms.Padding(0);
            this.txtLog.Name = "txtLog";
            this.txtLog.Size = new System.Drawing.Size(389, 302);
            this.txtLog.TabIndex = 14;
            this.txtLog.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Suppliers Portal";
            // 
            // lblDatabaseStatus
            // 
            this.lblDatabaseStatus.AutoSize = true;
            this.lblDatabaseStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabaseStatus.Location = new System.Drawing.Point(38, 203);
            this.lblDatabaseStatus.Name = "lblDatabaseStatus";
            this.lblDatabaseStatus.Size = new System.Drawing.Size(68, 13);
            this.lblDatabaseStatus.TabIndex = 12;
            this.lblDatabaseStatus.Text = "Connected";
            // 
            // imgDatabaseLive
            // 
            this.imgDatabaseLive.Image = global::MyOB.Sync.Properties.Resources.load_animation;
            this.imgDatabaseLive.Location = new System.Drawing.Point(55, 166);
            this.imgDatabaseLive.Name = "imgDatabaseLive";
            this.imgDatabaseLive.Size = new System.Drawing.Size(36, 34);
            this.imgDatabaseLive.TabIndex = 11;
            this.imgDatabaseLive.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Data Syncing";
            // 
            // dataSyncProgressBar
            // 
            this.dataSyncProgressBar.Location = new System.Drawing.Point(8, 57);
            this.dataSyncProgressBar.Name = "dataSyncProgressBar";
            this.dataSyncProgressBar.Size = new System.Drawing.Size(146, 23);
            this.dataSyncProgressBar.TabIndex = 8;
            this.dataSyncProgressBar.Value = 99;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 353);
            this.Controls.Add(this.panelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Dashboard";
            this.Text = "Suppliers Portal MyOB Sync";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Dashboard_FormClosing);
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.Resize += new System.EventHandler(this.Dashboard_Resize);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.panelHead.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgDatabaseLive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerCheckData;
        private System.Windows.Forms.NotifyIcon notifyIconMyOB;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Button btnCloseWindow;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDatabaseStatus;
        private System.Windows.Forms.PictureBox imgDatabaseLive;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar dataSyncProgressBar;
        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.Button btnMysqlServerSettings;
    }
}

