﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyOB.Sync.Properties;
using System.Configuration;
using System.Reflection;
using System.IO;

namespace MyOB.Sync
{
    public partial class CustomWindow : Form
    {
        string _path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\MyOB.Sync.exe";
        public CustomWindow()
        {
            InitializeComponent();
            this.ControlBox = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtGLNNumber.Text == "")
            {
                MessageBox.Show("Please enter a GLN number!");
            }

            else if (!isNumeric(txtGLNNumber.Text, System.Globalization.NumberStyles.Integer))
            {
                MessageBox.Show("Please enter a numeric GLN number only.");
                //MessageBox.Show("Please enter a valid numeric GLN");
            }
            else
            {
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(_path);
                // Add an Application Setting.
                config.AppSettings.Settings.Remove("GLNNumber");
                config.AppSettings.Settings.Add("GLNNumber", txtGLNNumber.Text);
                // Save the configuration file.
                config.Save(ConfigurationSaveMode.Modified);
                // Force a reload of a changed section.
                ConfigurationManager.RefreshSection("appSettings");

                this.Close();
            }
        }
        public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle,
                System.Globalization.CultureInfo.CurrentCulture, out result);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            String _GLNNumber = ConfigurationManager.AppSettings["GLNNumber"].ToString();
            MessageBox.Show(_GLNNumber);
            if (_GLNNumber != "")
            {
                if (txtGLNNumber.Text  != "")
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Please enter GLN number first.");
                }
            }
            else {
                MessageBox.Show("Please enter GLN number first.");
            }
        }
    }
}
