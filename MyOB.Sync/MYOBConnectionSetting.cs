﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyOB.Sync.Properties;
using MyOB.Interface;
using System.IO;
using System.Configuration;
using System.Reflection;
using ErrorLog;
using MyOB.BAL;

namespace MyOB.Sync
{
    public partial class MYOBConnectionSetting : Form
    {
        string _path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\MyOB.Sync.exe";
        AccountsClass _objAccountsClass;
        private Dashboard parentForm = null;
        public MYOBConnectionSetting()
        {
            InitializeComponent();
            
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            //Load saved settings 
            
           loadSettings();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                //submit logic
                btnSave.PerformClick();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void bindAccountSetting()
        {
            if (!checkValidation())
            {
                _objAccountsClass = new AccountsClass(ConnectionSetting.OpenMyObConnection());
                txtCostOfSaleAccount.DataSource = _objAccountsClass.getPurchasesAccounts();
                txtCostOfSaleAccount.ValueMember = "AccountNumber";
                txtCostOfSaleAccount.DisplayMember = "Name";

                txtIncomeAccountforTrackingSales.DataSource = _objAccountsClass.getSaleAccounts();
                txtIncomeAccountforTrackingSales.ValueMember = "AccountNumber";
                txtIncomeAccountforTrackingSales.DisplayMember = "Name";

                txtAssetAccountItemInventory.DataSource = _objAccountsClass.getTradeDebtorsAccounts();
                txtAssetAccountItemInventory.ValueMember = "AccountNumber";
                txtAssetAccountItemInventory.DisplayMember = "Name";
                _objAccountsClass = null;
            }
        }
        /// <summary>
        /// loadSettings values from setting file to form controles 
        /// </summary>
        private void loadSettings()
        {
            try
            {

                txtmyobfilePath.Text = Settings.Default.MYOBFilePath;
                txtUID.Text = Settings.Default.UID;
                txtPassword.Text = Settings.Default.PWD;
                txtHostExePath.Text = Settings.Default.MyoHostExePath;
                txtKey.Text = Settings.Default.MyobkeyfilePath;
                txtsalesperson.Text = Settings.Default.SalesPerson;
                txtShipVia.Text = Settings.Default.ShipVia;
                txtTaxCode.Text = Settings.Default.TaxCode;
                txtAddress.Text = Settings.Default.Address;
                
                if (Settings.Default.GLNNumber != "")
                {
                    txtGLNNumber.Text = Settings.Default.GLNNumber;
                }
                else
                {
                    Settings.Default.GLNNumber = txtGLNNumber.Text = ConfigurationManager.AppSettings["GLNNumber"].ToString();
                    Settings.Default.Save();
                }

                bindAccountSetting();

                DataTable _dataTable = (DataTable)txtIncomeAccountforTrackingSales.DataSource;
                int index = -1;
                if (Settings.Default.MYOBFilePath != null || Settings.Default.MYOBFilePath != string.Empty)
                {
                    DataRow[] rows = _dataTable.Select("AccountNumber Like '%" + Settings.Default.IncomeAccountforTrackingSales + "%'");
                    if (rows.Count() > 0)
                    {
                        index = _dataTable.Rows.IndexOf(rows[0]);
                        txtIncomeAccountforTrackingSales.SelectedIndex = index;
                    }


                    _dataTable = new DataTable();
                    _dataTable = (DataTable)txtAssetAccountItemInventory.DataSource;
                    index = -1;

                    DataRow[] rows1 = _dataTable.Select("AccountNumber Like '%" + Settings.Default.AssestAccountItemInventory + "%'");
                    if (rows1.Count() > 0)
                    {
                        index = _dataTable.Rows.IndexOf(rows1[0]);
                        txtAssetAccountItemInventory.SelectedIndex = index;
                    }



                    _dataTable = new DataTable();
                    _dataTable = (DataTable)txtCostOfSaleAccount.DataSource;
                    index = -1;
                    DataRow[] rows2 = _dataTable.Select("AccountNumber Like '%" + Settings.Default.CostOfSaleAccount + "%'");
                    if (rows2.Count() > 0)
                    {
                        index = _dataTable.Rows.IndexOf(rows2[0]);
                        txtCostOfSaleAccount.SelectedIndex = index;
                    }


                    _dataTable = null;
                }
               
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error on MYOConnectionSetting.cs in loadSettings function [MyOB.Sync]: " + ex.Message);
            }
        }
        /// <summary>
        /// Set Myob file path's button event
        /// Save Settings in application file 
        /// Reset form controles values 
        /// </summary>
        #region All buttons' events 
        /// Set Myob file path's button event
        private void btnmyobFile_Click(object sender, EventArgs e)
        {
            try
            {
                myobFileDialog.Filter = "allfiles|*.MYO";
                DialogResult _result = myobFileDialog.ShowDialog();
               
                if (_result == DialogResult.OK)
                {
                    if (Path.GetExtension(myobFileDialog.FileName).ToLower() == ".myo")
                    {
                        txtmyobfilePath.Text = myobFileDialog.FileName;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect file, please select only .myo file.");
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error on MYOConnectionSetting.cs in btnmyobFile_Click function[MyOB.Sync] : " + ex.Message);
            }
        }
        /// Save Settings in application file 
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkValidation())
                {
                    return;
                }

                Settings.Default.GLNNumber = txtGLNNumber.Text;
                Settings.Default.SalesPerson = txtsalesperson.Text;
                Settings.Default.ShipVia = txtShipVia.Text;
                Settings.Default.TaxCode = txtTaxCode.Text;
                Settings.Default.Address = txtAddress.Text;
                Settings.Default.Save();

                //
                string _key = string.Empty;
              // _key= txtKey.Text;
                //  return System.IO.File.ReadAllText(@"E:\Commit\MYOBPLOG.TXT");
                using (StreamReader streamReader = new StreamReader(txtKey.Text))
                {
                    _key = streamReader.ReadToEnd();
                    streamReader.Close();
                }

                if (!File.Exists(txtmyobfilePath.Text))
                {
                    MessageBox.Show("MYOB data file not exists, please check file path.");
                    txtmyobfilePath.Focus();
                    errorProvider.SetError(txtmyobfilePath, "MYOB data file not exists, please check file path.");
                    return ;
                }

                // Open App.Config of executable
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(_path);
                // Add an Application Setting.
                config.AppSettings.Settings.Remove("GLNNumber");
                config.AppSettings.Settings.Add("GLNNumber", txtGLNNumber.Text);
                // Save the configuration file.
                config.Save(ConfigurationSaveMode.Modified);
                // Force a reload of a changed section.
                ConfigurationManager.RefreshSection("appSettings");


                ConnectionCLS _objConnection = new ConnectionCLS();
                MYOConnectionSetting _objSetting = new MYOConnectionSetting();
                _objSetting.MYOBFilePath = txtmyobfilePath.Text;
                _objSetting.UID = txtUID.Text;
                _objSetting.PWD = txtPassword.Text;
                _objSetting.MYOBHostExePath = txtHostExePath.Text;
                _objSetting.MYOBKey = _key;

                string connectionResult;
                if (!_objConnection.ConnectionOpen(_objSetting, out  connectionResult))
                {
                    _objConnection.ConnectionClose();
                    MessageBox.Show(connectionResult);
                }
                else
                {
                    Settings.Default.MYOBFilePath = txtmyobfilePath.Text;
                    Settings.Default.UID = txtUID.Text;
                    Settings.Default.PWD = txtPassword.Text;
                    Settings.Default.MyoHostExePath = txtHostExePath.Text;
                    Settings.Default.MyoKey = _key;
                    Settings.Default.MyobkeyfilePath = txtKey.Text;
                    Settings.Default.GLNNumber = txtGLNNumber.Text;
                    Settings.Default.Save();
                    _objConnection.ConnectionClose();
                    MessageBox.Show("Settings saved successfully.");
                    loadSettings();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ErrorClass.WriteError("Error in MYOConnectionSetting.cs, function name btnSave_Click [MyOB.Sync]:-" + ex.Message);
               // Console.WriteLine("Error on MYOConnectionSetting.cs in btnSave_Click function : " + ex.Message);
            }
        }
        /// Reset form controles values 
        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtmyobfilePath.Text = "";
                txtPassword.Text = "";
                txtUID.Text = "";
                txtHostExePath.Text = "";
                txtKey.Text = "";
                txtGLNNumber.Text = "";
                txtsalesperson.Text = "";
                txtShipVia.Text = "";
                txtAddress.Text = "";
                txtTaxCode.Text = "";
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error on MYOConnectionSetting.cs in btnReset_Click function [MyOB.Sync]: " + ex.Message);
                
            }
        }

        private bool checkValidation()
        {
            bool _return = false;
            if (txtKey.Text == "")
            {
                errorProvider.SetError(txtKey, "Please enter the MYOB Key.");
                _return = true;
            }

            if (txtmyobfilePath.Text == "")
            {
                errorProvider.SetError(txtmyobfilePath, "Please enter the MYOB file path.");
                _return = true;
            }


            if (txtUID.Text == "")
            {
                errorProvider.SetError(txtUID, "Please enter the user id.");
                _return = true;
            }

            if (txtGLNNumber.Text == "")
            {
                errorProvider.SetError(txtGLNNumber, "Please enter the GLN Number.");
                _return = true;
            }

            if (txtsalesperson.Text == "")
            {
                errorProvider.SetError(txtsalesperson, "Please enter the sales person name.");
                _return = true;
            }

            if (txtTaxCode.Text == "")
            {
                errorProvider.SetError(txtTaxCode, "Please enter the TAX code.");
                _return = true;
            }

            if (txtShipVia.Text == "")
            {
                errorProvider.SetError(txtShipVia, "Please enter the ship via.");
                _return = true;
            }

            return _return;
        }
        #endregion
        //
        private void btnHostExePath_Click(object sender, EventArgs e)
        {
            try
            {
                myobFileDialog.Filter = "All files (*.*)|*.*";
                DialogResult _result = myobFileDialog.ShowDialog();
                myobFileDialog.Filter = null;
                if (_result == DialogResult.OK)
                {
                    if (Path.GetFileName(myobFileDialog.FileName).ToLower() == "myobp.exe")
                    {
                        txtHostExePath.Text = myobFileDialog.FileName;
                    }
                    else if (Path.GetFileName(myobFileDialog.FileName).ToLower() == "myob.exe")
                    {
                        txtHostExePath.Text = myobFileDialog.FileName;
                    } 
                    else
                    {
                        MessageBox.Show("Incorrect file, please select only MYOBP.exe/Myob.exe file.");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error on MYOConnectionSetting.cs in btnHostExePath_Click function [MyOB.Sync]: " + ex.Message);
            }
        }

        private void btnKeyfile_Click(object sender, EventArgs e)
        {
            try
            {
                myobFileDialog.Filter = "All files (*.*)|*.*";
                DialogResult _result = myobFileDialog.ShowDialog();
                   myobFileDialog.Filter = null;
                if (_result == DialogResult.OK)
                {
                    txtKey.Text = myobFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error on MYOConnectionSetting.cs in btnHostExePath_Click function [MyOB.Sync]: " + ex.Message);
            }
        }

        private void btnSaveAccountsSetting_Click(object sender, EventArgs e)
        {
            //txtIncomeAccountforTrackingSales
            //txtAssestAccountItemInvento
            //txtCostOfSaleAccount
            if (txtIncomeAccountforTrackingSales.SelectedIndex > 0)
            {
                Settings.Default.IncomeAccountforTrackingSales = txtIncomeAccountforTrackingSales.SelectedValue.ToString();
            }
            if (txtAssetAccountItemInventory.SelectedIndex > 0)
            {
                Settings.Default.AssestAccountItemInventory = txtAssetAccountItemInventory.SelectedValue.ToString();
            }
            if (txtCostOfSaleAccount.SelectedIndex > 0)
            {
                Settings.Default.CostOfSaleAccount = txtCostOfSaleAccount.SelectedValue.ToString();
            }
            
            Settings.Default.Save();

            MessageBox.Show("Accounts settings saved successfully.");
            if (parentForm != null)
                parentForm.Refresh();
        }

        private void btnAccountsReset_Click(object sender, EventArgs e)
        {
            txtIncomeAccountforTrackingSales.Text = "";
            txtAssetAccountItemInventory.Text = "";
            txtCostOfSaleAccount.Text = "";
        }

    }
}
