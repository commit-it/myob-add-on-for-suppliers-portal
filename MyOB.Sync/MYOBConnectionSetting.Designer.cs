﻿namespace MyOB.Sync
{
    partial class MYOBConnectionSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MYOBConnectionSetting));
            this.myobFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblMYOBFilePath = new System.Windows.Forms.Label();
            this.txtmyobfilePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnmyobFile = new System.Windows.Forms.Button();
            this.btnHostExePath = new System.Windows.Forms.Button();
            this.btnKeyfile = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.lblHostExePath = new System.Windows.Forms.Label();
            this.txtHostExePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblsalesperson = new System.Windows.Forms.Label();
            this.txtsalesperson = new System.Windows.Forms.TextBox();
            this.lblshipvia = new System.Windows.Forms.Label();
            this.txtShipVia = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTaxCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGLNNumber = new System.Windows.Forms.TextBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAssetAccountItemInventory = new System.Windows.Forms.ComboBox();
            this.txtIncomeAccountforTrackingSales = new System.Windows.Forms.ComboBox();
            this.txtCostOfSaleAccount = new System.Windows.Forms.ComboBox();
            this.btnAccountsReset = new System.Windows.Forms.Button();
            this.btnSaveAccountsSetting = new System.Windows.Forms.Button();
            this.txtAssetAccountItemInventory1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIncomeAccountforTrackingSales1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCostOfSaleAccount1 = new System.Windows.Forms.TextBox();
            this.lblCostofSaleAccount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // lblMYOBFilePath
            // 
            this.lblMYOBFilePath.AutoSize = true;
            this.lblMYOBFilePath.Location = new System.Drawing.Point(13, 35);
            this.lblMYOBFilePath.Name = "lblMYOBFilePath";
            this.lblMYOBFilePath.Size = new System.Drawing.Size(82, 13);
            this.lblMYOBFilePath.TabIndex = 0;
            this.lblMYOBFilePath.Text = "MYOB File Path";
            // 
            // txtmyobfilePath
            // 
            this.txtmyobfilePath.Location = new System.Drawing.Point(168, 33);
            this.txtmyobfilePath.Name = "txtmyobfilePath";
            this.txtmyobfilePath.Size = new System.Drawing.Size(218, 20);
            this.txtmyobfilePath.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Login ID";
            // 
            // txtUID
            // 
            this.txtUID.Location = new System.Drawing.Point(168, 59);
            this.txtUID.Name = "txtUID";
            this.txtUID.Size = new System.Drawing.Size(218, 20);
            this.txtUID.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(168, 85);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(218, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // btnmyobFile
            // 
            this.btnmyobFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmyobFile.Location = new System.Drawing.Point(393, 32);
            this.btnmyobFile.Name = "btnmyobFile";
            this.btnmyobFile.Size = new System.Drawing.Size(75, 23);
            this.btnmyobFile.TabIndex = 2;
            this.btnmyobFile.Text = "Select";
            this.btnmyobFile.UseVisualStyleBackColor = true;
            this.btnmyobFile.Click += new System.EventHandler(this.btnmyobFile_Click);
            // 
            // btnHostExePath
            // 
            this.btnHostExePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHostExePath.Location = new System.Drawing.Point(393, 110);
            this.btnHostExePath.Name = "btnHostExePath";
            this.btnHostExePath.Size = new System.Drawing.Size(75, 23);
            this.btnHostExePath.TabIndex = 6;
            this.btnHostExePath.Text = "Select";
            this.btnHostExePath.UseVisualStyleBackColor = true;
            this.btnHostExePath.Click += new System.EventHandler(this.btnHostExePath_Click);
            // 
            // btnKeyfile
            // 
            this.btnKeyfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKeyfile.Location = new System.Drawing.Point(393, 139);
            this.btnKeyfile.Name = "btnKeyfile";
            this.btnKeyfile.Size = new System.Drawing.Size(75, 23);
            this.btnKeyfile.TabIndex = 8;
            this.btnKeyfile.Text = "Select";
            this.btnKeyfile.UseVisualStyleBackColor = true;
            this.btnKeyfile.Click += new System.EventHandler(this.btnKeyfile_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(146, 305);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(185, 23);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save MYOB Database Settings";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(337, 306);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 15;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblHostExePath
            // 
            this.lblHostExePath.AutoSize = true;
            this.lblHostExePath.Location = new System.Drawing.Point(12, 113);
            this.lblHostExePath.Name = "lblHostExePath";
            this.lblHostExePath.Size = new System.Drawing.Size(128, 13);
            this.lblHostExePath.TabIndex = 7;
            this.lblHostExePath.Text = "AccountRight Executable";
            // 
            // txtHostExePath
            // 
            this.txtHostExePath.Location = new System.Drawing.Point(168, 111);
            this.txtHostExePath.Name = "txtHostExePath";
            this.txtHostExePath.Size = new System.Drawing.Size(218, 20);
            this.txtHostExePath.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Key Value";
            // 
            // lblsalesperson
            // 
            this.lblsalesperson.AutoSize = true;
            this.lblsalesperson.Location = new System.Drawing.Point(13, 202);
            this.lblsalesperson.Name = "lblsalesperson";
            this.lblsalesperson.Size = new System.Drawing.Size(69, 13);
            this.lblsalesperson.TabIndex = 12;
            this.lblsalesperson.Text = "Sales Person";
            // 
            // txtsalesperson
            // 
            this.txtsalesperson.Location = new System.Drawing.Point(168, 202);
            this.txtsalesperson.Name = "txtsalesperson";
            this.txtsalesperson.Size = new System.Drawing.Size(218, 20);
            this.txtsalesperson.TabIndex = 10;
            // 
            // lblshipvia
            // 
            this.lblshipvia.AutoSize = true;
            this.lblshipvia.Location = new System.Drawing.Point(13, 228);
            this.lblshipvia.Name = "lblshipvia";
            this.lblshipvia.Size = new System.Drawing.Size(45, 13);
            this.lblshipvia.TabIndex = 12;
            this.lblshipvia.Text = "Ship via";
            // 
            // txtShipVia
            // 
            this.txtShipVia.Location = new System.Drawing.Point(168, 228);
            this.txtShipVia.Name = "txtShipVia";
            this.txtShipVia.Size = new System.Drawing.Size(218, 20);
            this.txtShipVia.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 254);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "TAX Code";
            // 
            // txtTaxCode
            // 
            this.txtTaxCode.Location = new System.Drawing.Point(168, 254);
            this.txtTaxCode.Name = "txtTaxCode";
            this.txtTaxCode.Size = new System.Drawing.Size(218, 20);
            this.txtTaxCode.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "GLN Number";
            // 
            // txtGLNNumber
            // 
            this.txtGLNNumber.Location = new System.Drawing.Point(168, 174);
            this.txtGLNNumber.Name = "txtGLNNumber";
            this.txtGLNNumber.Size = new System.Drawing.Size(218, 20);
            this.txtGLNNumber.TabIndex = 9;
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(168, 139);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(218, 20);
            this.txtKey.TabIndex = 7;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(13, 280);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(45, 13);
            this.lblAddress.TabIndex = 12;
            this.lblAddress.Text = "Address";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(168, 280);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(218, 20);
            this.txtAddress.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAssetAccountItemInventory);
            this.groupBox1.Controls.Add(this.txtIncomeAccountforTrackingSales);
            this.groupBox1.Controls.Add(this.txtCostOfSaleAccount);
            this.groupBox1.Controls.Add(this.btnAccountsReset);
            this.groupBox1.Controls.Add(this.btnSaveAccountsSetting);
            this.groupBox1.Controls.Add(this.txtAssetAccountItemInventory1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtIncomeAccountforTrackingSales1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtCostOfSaleAccount1);
            this.groupBox1.Controls.Add(this.lblCostofSaleAccount);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(7, 348);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(514, 143);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Accounts Settings";
            // 
            // txtAssetAccountItemInventory
            // 
            this.txtAssetAccountItemInventory.FormattingEnabled = true;
            this.txtAssetAccountItemInventory.Location = new System.Drawing.Point(189, 72);
            this.txtAssetAccountItemInventory.Name = "txtAssetAccountItemInventory";
            this.txtAssetAccountItemInventory.Size = new System.Drawing.Size(197, 21);
            this.txtAssetAccountItemInventory.TabIndex = 27;
            // 
            // txtIncomeAccountforTrackingSales
            // 
            this.txtIncomeAccountforTrackingSales.FormattingEnabled = true;
            this.txtIncomeAccountforTrackingSales.Location = new System.Drawing.Point(189, 45);
            this.txtIncomeAccountforTrackingSales.Name = "txtIncomeAccountforTrackingSales";
            this.txtIncomeAccountforTrackingSales.Size = new System.Drawing.Size(197, 21);
            this.txtIncomeAccountforTrackingSales.TabIndex = 26;
            // 
            // txtCostOfSaleAccount
            // 
            this.txtCostOfSaleAccount.FormattingEnabled = true;
            this.txtCostOfSaleAccount.Location = new System.Drawing.Point(189, 19);
            this.txtCostOfSaleAccount.Name = "txtCostOfSaleAccount";
            this.txtCostOfSaleAccount.Size = new System.Drawing.Size(197, 21);
            this.txtCostOfSaleAccount.TabIndex = 25;
            // 
            // btnAccountsReset
            // 
            this.btnAccountsReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccountsReset.Location = new System.Drawing.Point(410, 104);
            this.btnAccountsReset.Name = "btnAccountsReset";
            this.btnAccountsReset.Size = new System.Drawing.Size(75, 23);
            this.btnAccountsReset.TabIndex = 24;
            this.btnAccountsReset.Text = "Reset";
            this.btnAccountsReset.UseVisualStyleBackColor = true;
            this.btnAccountsReset.Visible = false;
            this.btnAccountsReset.Click += new System.EventHandler(this.btnAccountsReset_Click);
            // 
            // btnSaveAccountsSetting
            // 
            this.btnSaveAccountsSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveAccountsSetting.Location = new System.Drawing.Point(189, 104);
            this.btnSaveAccountsSetting.Name = "btnSaveAccountsSetting";
            this.btnSaveAccountsSetting.Size = new System.Drawing.Size(136, 23);
            this.btnSaveAccountsSetting.TabIndex = 23;
            this.btnSaveAccountsSetting.Text = "Save Accounts Settings";
            this.btnSaveAccountsSetting.UseVisualStyleBackColor = true;
            this.btnSaveAccountsSetting.Click += new System.EventHandler(this.btnSaveAccountsSetting_Click);
            // 
            // txtAssetAccountItemInventory1
            // 
            this.txtAssetAccountItemInventory1.Location = new System.Drawing.Point(392, 72);
            this.txtAssetAccountItemInventory1.Name = "txtAssetAccountItemInventory1";
            this.txtAssetAccountItemInventory1.Size = new System.Drawing.Size(113, 20);
            this.txtAssetAccountItemInventory1.TabIndex = 22;
            this.txtAssetAccountItemInventory1.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Asset Account for Item Inventory";
            // 
            // txtIncomeAccountforTrackingSales1
            // 
            this.txtIncomeAccountforTrackingSales1.Location = new System.Drawing.Point(392, 44);
            this.txtIncomeAccountforTrackingSales1.Name = "txtIncomeAccountforTrackingSales1";
            this.txtIncomeAccountforTrackingSales1.Size = new System.Drawing.Size(113, 20);
            this.txtIncomeAccountforTrackingSales1.TabIndex = 20;
            this.txtIncomeAccountforTrackingSales1.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Income Account for Tracking Sales";
            // 
            // txtCostOfSaleAccount1
            // 
            this.txtCostOfSaleAccount1.Location = new System.Drawing.Point(392, 18);
            this.txtCostOfSaleAccount1.Name = "txtCostOfSaleAccount1";
            this.txtCostOfSaleAccount1.Size = new System.Drawing.Size(113, 20);
            this.txtCostOfSaleAccount1.TabIndex = 18;
            this.txtCostOfSaleAccount1.Visible = false;
            // 
            // lblCostofSaleAccount
            // 
            this.lblCostofSaleAccount.AutoSize = true;
            this.lblCostofSaleAccount.Location = new System.Drawing.Point(17, 21);
            this.lblCostofSaleAccount.Name = "lblCostofSaleAccount";
            this.lblCostofSaleAccount.Size = new System.Drawing.Size(104, 13);
            this.lblCostofSaleAccount.TabIndex = 17;
            this.lblCostofSaleAccount.Text = "Cost of sale account";
            // 
            // MYOBConnectionSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 503);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.txtGLNNumber);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTaxCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtShipVia);
            this.Controls.Add(this.lblshipvia);
            this.Controls.Add(this.txtsalesperson);
            this.Controls.Add(this.lblsalesperson);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtHostExePath);
            this.Controls.Add(this.lblHostExePath);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnKeyfile);
            this.Controls.Add(this.btnHostExePath);
            this.Controls.Add(this.btnmyobFile);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtmyobfilePath);
            this.Controls.Add(this.lblMYOBFilePath);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MYOBConnectionSetting";
            this.Text = "Myob settings";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog myobFileDialog;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.TextBox txtGLNNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTaxCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtShipVia;
        private System.Windows.Forms.Label lblshipvia;
        private System.Windows.Forms.TextBox txtsalesperson;
        private System.Windows.Forms.Label lblsalesperson;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtHostExePath;
        private System.Windows.Forms.Label lblHostExePath;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnKeyfile;
        private System.Windows.Forms.Button btnHostExePath;
        private System.Windows.Forms.Button btnmyobFile;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtmyobfilePath;
        private System.Windows.Forms.Label lblMYOBFilePath;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCostOfSaleAccount1;
        private System.Windows.Forms.Label lblCostofSaleAccount;
        private System.Windows.Forms.TextBox txtAssetAccountItemInventory1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIncomeAccountforTrackingSales1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSaveAccountsSetting;
        private System.Windows.Forms.Button btnAccountsReset;
        private System.Windows.Forms.ComboBox txtCostOfSaleAccount;
        private System.Windows.Forms.ComboBox txtAssetAccountItemInventory;
        private System.Windows.Forms.ComboBox txtIncomeAccountforTrackingSales;
    }
}