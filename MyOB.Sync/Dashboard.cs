﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyOB.Interface;
using MyOB.Sync.Properties;
using System.IO;
using MyOB.BAL;
using System.Configuration;
using System.Threading;
using System.Security.Principal;
using System.Diagnostics;
using ErrorLog;
using System.Runtime.InteropServices;

namespace MyOB.Sync
{
    public partial class Dashboard : Form
    {
        ConnectionCLS _objConnection = new ConnectionCLS();
        MySQLDataSyncClass _objMySQLDataSyncClass;
        myobDataImportValues _myobDataImportValues = new myobDataImportValues();
        //If live work in process _lockRequest=true 
        bool _lockRequest = false;
        static bool _formCloseRequest = false;
        string connectionResult;
        //check for myob and mysql database setting 
        static bool _allSettingWorking = false;

        string _clearMessage;

        public Dashboard()
        {
            //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //InitializeComponent();
            //ProcessHelper ph = new ProcessHelper();
            //ph.KillProcessByNameAndUser("Myobp");

            _clearMessage = "Something wrong with myob connection,please exit from application and do following steps :-" + Environment.NewLine;
            _clearMessage += "Please remove .flk file from myob file path {0}" + Environment.NewLine;
            _clearMessage += "Please remove .box file from myob file path {1}" + Environment.NewLine;

                //if (!IsAdministrator())
             
                  if (!IsAdministrator())
                    {
                        MessageBox.Show("Please run application as administrator!");
                        this.Close();
                    }
                    else
                    {
                        if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1)
                        {
                            MessageBox.Show("Application is alredy running");
                            this.Close();
                        }
                        else
                        {
                           /// run application with admin user
                          //  Thread.Sleep(2000);
                            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                            InitializeComponent();

                            notifyIconMyOB.ContextMenuStrip = new ContextMenus().Create();
                            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
                          
                            dataSyncProgressBar.Style = ProgressBarStyle.Marquee;
                            dataSyncProgressBar.MarqueeAnimationSpeed =30;

                            try
                            {
                                Setlog("Connecting.....");
                                //  Check myob conntection setting on load 
                               // MyObconnection();
                                //  Start timer tick event
                                timerCheckData.Enabled = true;
                               // #################--call import invoice function on form load for testing --###############
                              // CallMySQLImportInvoiceFunction(); 

                            }
                            catch (Exception ex)
                            {
                                ErrorClass.WriteError("## Error on Dashboard.cs in Dashboard function [MyOB.Sync]: " + ex.Message);
                                MessageBox.Show(ex.Message);
                            }
                        }
                  }//end if
            
        }
      
       

        //Check MySQLconnection connectin settings
        private bool MySQLconnection()
        {
            try
            {
                string _message = string.Empty;
                if (!_objConnection.MySQlConnectionOpen(ConnectionSetting.mySQLConnectionSetting(),out _message))
                {
                    //MessageBox.Show(_message);
                    //set offline image if live database not connected
                   

                    //set progress bar staus on database offline 
                    this.Invoke(new MethodInvoker(delegate()
                    {
                        imgDatabaseLive.Image = new Bitmap(Resources.database_offline);
                      //  dataSyncProgressBar.Visible = false;
                        this.dataSyncProgressBar.Value = 0;
                        //Access your controls
                    }));
                    Setlog("## Error: Please check in MySQL setting window." + _message);
                    SetStatus("   Offline  ");
                    return false;
                }
                else
                {
                    Setlog(_message);
                    _allSettingWorking = true;
                  //  _lockRequest = false;
                    //set online image
                   
                    
                    this.Invoke(new MethodInvoker(delegate()
                    {
                       // imgDatabaseLive.Image = new Bitmap(Resources.load_animation);
                        //dataSyncProgressBar.Visible = true;
                        this.dataSyncProgressBar.Value = 99;
                        //Access your controls
                    }));

                    SetStatus("Connected");
                    return true;
                }
            }
            catch (Exception ex)
            {
             
                _lockRequest = false;
                ErrorClass.WriteError("Error on Dashboard.cs in MySQLconnection function [MyOB.Sync] : " + ex.Message);
                return false;
            }
        }
        //Check MyOb file connection settings 
        private void MyObconnection()
        {
            try
            {
                if (Settings.Default.MYOBFilePath != null && Settings.Default.MYOBFilePath != "")
                {
                   
                    if (!_objConnection.ConnectionOpen(ConnectionSetting.OpenMyObConnection(),out  connectionResult))
                    {
                       // MessageBox.Show("MYOB login details are incorrect.");
                        Setlog(connectionResult);
                        //Setlog("MYOB login details are incorrect, please check in MyOB setting window.");
                        //MYOBConnectionSetting showForm = new MYOBConnectionSetting();
                        //showForm.ShowDialog();
                    }
                    else
                    {
                        Setlog("MYOB database successfully connected.");
                       MySQLconnection();
                    }
                }
                else
                {
                    //MessageBox.Show("Please set MYOB file path and login details first.");
                    Setlog("Please set MYOB file path and login details first.");
                    MYOBConnectionSetting showForm = new MYOBConnectionSetting();
                    showForm.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                _lockRequest = false;
                ErrorClass.WriteError("Error on Dashboard.cs in MyObconnection function [MyOB.Sync] : " + ex.Message);
                 //Console.WriteLine("Error on Dashboard.cs in MyObconnection function : "+ex.Message);
            }            

        }

        delegate void SetTextCallback(string text);
        //write log
        private void Setlog(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.txtLog.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(Setlog);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.txtLog.Text = this.txtLog.Text + "[" + DateTime.Now + "] " + text + Environment.NewLine;
            }
        }

        //set database status on timer 
        delegate void SetLableTextCallback(string text);
        //write log
        private void SetStatus(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.lblDatabaseStatus.InvokeRequired)
            {
                SetLableTextCallback d = new SetLableTextCallback(SetStatus);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.lblDatabaseStatus.Text = text;
            }
        }

        //Open MYOB file settings form
        private void btnSetting_Click(object sender, EventArgs e)
        {
            try
            {
                MYOBConnectionSetting showForm = new MYOBConnectionSetting();
                showForm.Show();
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error on Dashboard.cs in btnSetting_Click function[MyOB.Sync] : " + ex.Message);
            }
        }
        /// <summary>
        /// Timer Tick function for data syncing
        /// </summary>
        private void timerCheckData_Tick(object sender, EventArgs e)
        {
            try
            {
              
                ImportInvoiceFromMySQL();
               
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error on Dashboard.cs in timerCheckData_Tick function [MyOB.Sync] : " + ex.Message);
            }
        }

        private void ImportInvoiceFromMySQL()
        {

            //check request is alredy in queue
            try
            {  
                //set timing
                setInterval();
                //start syncing 
                if (!_lockRequest)
                {
                    _lockRequest = true;
                    //show terminal window on start process
                    this.Show();
                    this.WindowState = FormWindowState.Normal;

                    Thread t = new Thread(new ThreadStart(CallMySQLImportInvoiceFunction));
                    t.IsBackground = true;
                    t.Start();
                    t = null;
                }
            }
            catch (Exception ex)
            {
                _lockRequest = false;
                ErrorClass.WriteError("Error on Dashboard.cs in ImportInvoiceFromMySQL function [MyOB.Sync] : " + ex.Message);
                MessageBox.Show(ex.Message);
            }
        }

        private void CallMySQLImportInvoiceFunction()
        {
            try
            {
                

                   if (!MainClass.TcpSocketTest())
                   {
                      
                       Setlog("## Error:- internet not working on your system.");
                      
                       //set offline image if live database not connected
                       imgDatabaseLive.Image = new Bitmap(Resources.database_offline);

                       //set progress bar staus on database offline 
                       this.Invoke(new MethodInvoker(delegate()
                       {
                           //  dataSyncProgressBar.Visible = false;
                           this.dataSyncProgressBar.Value = 0;
                           //Access your controls
                       }));
                       SetStatus("   Offline  ");
                       _lockRequest = false;
                       return;
                   }
                   else
                   {
                      
                       //set online image
                       imgDatabaseLive.Image = new Bitmap(Resources.load_animation);

                       this.Invoke(new MethodInvoker(delegate()
                       {
                           //dataSyncProgressBar.Visible = true;
                           this.dataSyncProgressBar.Value = 99;
                           //Access your controls
                       }));
                       SetStatus("Connected");

                   }

                   _myobDataImportValues.address = Settings.Default.Address;
                   _myobDataImportValues.localLOGfile = Settings.Default.MYOBFilePath;
                   _myobDataImportValues.salesPerson = Settings.Default.SalesPerson;
                   _myobDataImportValues.shipVia = Settings.Default.ShipVia;
                   _myobDataImportValues.tax = Settings.Default.TaxCode;
                   _myobDataImportValues.glnNumber = Settings.Default.GLNNumber;
                   _myobDataImportValues.MyoHostExePath = Settings.Default.MyoHostExePath;
                   try
                   {
                       _myobDataImportValues.AssestAccountItemInvento = Convert.ToInt32(Settings.Default.AssestAccountItemInventory.Replace("-", ""));
                       _myobDataImportValues.IncomeAccountforTrackingSales = Convert.ToInt32(Settings.Default.IncomeAccountforTrackingSales.Replace("-", ""));
                       _myobDataImportValues.CostOfSaleAccount = Convert.ToInt32(Settings.Default.CostOfSaleAccount.Replace("-", ""));
                   }
                   catch (Exception)
                   {
                       Setlog("Please check account setting values");
                       _lockRequest = false;
                       return;
                   }
                //check account settings 
                   if (!checkAccountSettings())
                   {

                       _lockRequest = false;
                       //2 minute for recall 120000
                       timerCheckData.Stop();
                       timerCheckData.Interval = 30000;
                       timerCheckData.Start();
                       return;
                   }
                   else
                   {
 
                   }

  
                          // Setlog("Hello I am working :)");
                          if (Settings.Default.GLNNumber == "")
                          {
                              Setlog("GLN number detail incorrect, please check from setting window.");
                              _lockRequest = false;
                              timerCheckData.Interval = 30000;
                              //9377779098035
                              return;
                          }
                          if (Settings.Default.MYOBFilePath != null && Settings.Default.MYOBFilePath != "")
                          {

                              if (!_objConnection.ConnectionOpen(ConnectionSetting.OpenMyObConnection(), out  connectionResult))
                              {
                                  //Setlog("MYOB login details are incorrect, please check from setting window.");
                                  Setlog(connectionResult);
                                  _lockRequest = false;
                                  //2 minute for recall 
                                  timerCheckData.Interval = 30000;
                                  return;
                              }
                              else
                              {
                                  Setlog(connectionResult);

                                  if (!MySQLconnection())
                                  {
                                      //2 minute for recall 
                                      timerCheckData.Interval = 30000;
                                  }
                              }
                          }
                          else
                          {
                              Setlog("Please set MYOB file path and login details first.");
                              _lockRequest = false;
                              return;
                          }
                          //If all setting working ok then call importInvoice function 
                 if (_allSettingWorking)
                      {
                        

                         

                          _objConnection.ConnectionClose();
                          _objConnection.MySQlConnectionClose();
                          _objMySQLDataSyncClass = new MySQLDataSyncClass(ConnectionSetting.OpenMyObConnection(), ConnectionSetting.mySQLConnectionSetting(), Settings.Default.GLNNumber);
                          //Settings.Default.MYOBFilePath, Settings.Default.GLNNumber, Settings.Default.TaxCode, Settings.Default.SalesPerson, Settings.Default.ShipVia, Settings.Default.Address
                          //Call import invoice function
                          Setlog("Bunnings Orders Processed");
                          string _logMessage=string.Empty;
                          string _status = string.Empty;
                           _status = _objMySQLDataSyncClass.importInvoice(_myobDataImportValues, out _logMessage);
                         

                          

                          if (_status != "0")
                          {
                              Setlog(string.Format(_clearMessage, Settings.Default.MYOBFilePath, Settings.Default.MYOBFilePath));
                          }
                          else
                          {
                              Setlog(_logMessage);
                          }
                          //Setlog("bye for now :)");
                      
                       /*   Setlog("Updated invoices import and export inprocess");
                          _status = _objMySQLDataSyncClass.importUpdatedInvoice(_myobDataImportValues, out _logMessage);

                          if (_status != "0")
                          {
                              Setlog("## Error :-" + _logMessage);
                          }
                          else
                          {
                              Setlog(_logMessage);
                          }*/
                          //Close pending database connection 
                          _objConnection.ConnectionClose();
                          _objConnection.MySQlConnectionClose();
                          _objMySQLDataSyncClass = null;
                          _lockRequest = false;

                     //close application,if user try to close app from close button
                          if (_formCloseRequest)
                          {
                              Application.Exit();
                          }
                         }
                         else
                         {
                            // MyObconnection();
                             _lockRequest = false;
                         }

            }
            catch (Exception ex)
            {
                _objConnection.ConnectionClose();
                _objConnection.MySQlConnectionClose();
                _lockRequest = false;
               // Setlog("## Error :- CallMySQLImportInvoiceFunction Error " + ex.Message);
            }
          
        }

        private Boolean checkAccountSettings()
        {
            if (_myobDataImportValues.AssestAccountItemInvento == 0 )
            {
                Setlog("Assest Account Item Inventory is empty,please check from myob setting window.");
                return false;
            }
            if (_myobDataImportValues.CostOfSaleAccount == 0 )
            {
                Setlog("Cost Of Sale Account is empty,please check from myob setting window.");
                return false;
            }
             if ( _myobDataImportValues.IncomeAccountforTrackingSales==0)
            {
                Setlog("Income Account for Tracking Sales is empty,please check from myob setting window.");
                return false;
            }
         
                return true;
            
        }
        /// <summary>
        /// with probably 2 intervals: every 15 min from 8 am to 11 am and the rest of the day hourly
        /// </summary>
        private void setInterval()
        {
            //15 min=900000 millisecond
            //60 min=3600000 millisecond

            try
            {
                //timerCheckData.Stop();
                    // convert everything to TimeSpan
                    TimeSpan start = new TimeSpan(08, 0, 0);
                    TimeSpan end = new TimeSpan(11, 0, 0);
                    bool setTimer = TimeBetween(DateTime.Now, start, end);
                    if (setTimer)
                    {
                        timerCheckData.Interval = 900000;
                        
                    }
                    else
                    {

                        //if (timerCheckData.Interval != 600000)
                        //{
                        //    timerCheckData.Interval = 600000;
                        //}

                        if (timerCheckData.Interval != 3600000)
                        {
                            timerCheckData.Interval = 3600000;
                        }
                    }
                  //  timerCheckData.Start();
            }
            catch (Exception)
            {
               // timerCheckData.Start();
              //  throw;
            }
        }

        // check timing 
        bool TimeBetween(DateTime datetime, TimeSpan start, TimeSpan end)
        {
            // convert datetime to a TimeSpan
            TimeSpan now = datetime.TimeOfDay;
            // see if start comes before end
            if (start < end)
                return start <= now && now <= end;
            // start is after end, so do the inverse comparison
            return !(end < now && now < start);
        }
        /// <summary>
        /// Check app environment runing as with admin user or not.
        /// </summary>
        /// <returns>bool value</returns>
        public bool IsAdministrator()
        {
            return (new WindowsPrincipal(WindowsIdentity.GetCurrent()))
                    .IsInRole(WindowsBuiltInRole.Administrator);
        }


        

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void btnCloseWindow_Click(object sender, EventArgs e)
        {
            activeNotifyIcon();
        }
        private void activeNotifyIcon()
        {
            notifyIconMyOB.BalloonTipTitle = "Suppliers Portal MyOB Sync";
            notifyIconMyOB.BalloonTipText = "You have to click for open app.";
            notifyIconMyOB.Visible = true;
            
            notifyIconMyOB.ShowBalloonTip(500);
            this.Hide();
            
        }

        private void btnMysqlServerSettings_Click(object sender, EventArgs e)
        {
            try
            {
                Login loginPopup = new Login();
                DialogResult dialogresult = loginPopup.ShowDialog();
                if (dialogresult == DialogResult.OK)
                {
                    if (loginPopup.isLogin)
                    {
                        loginPopup.Dispose();
                        MySQLDataBaseSettings showForm = new MySQLDataBaseSettings();
                        showForm.Show();
                    }

                }
            else if (dialogresult == DialogResult.Cancel)
            {
              
                loginPopup.Dispose();
            }
                
            }
            catch (Exception ex)
            {
                ErrorClass.WriteError("Error on Dashboard.cs in btnMysqlServerSettings_Click function[MyOB.Sync] : " + ex.Message);
            }
        }

        private void Dashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (_lockRequest)
                {
                    _formCloseRequest = true;
                    MessageBox.Show("Bunnings orders inprocess, application will close automatically after completing the process.");
                      e.Cancel = true;
                    //_objConnection.ConnectionClose();
                    //  _objConnection.MySQlConnectionClose();
                }
                else
                {
                    _objConnection.ConnectionClose();
                    _objConnection.MySQlConnectionClose();
                }
            }
            catch (Exception)
            {
                
                
            }
            //if (e.CloseReason == CloseReason.UserClosing)
            //{
            //    e.Cancel = true;
            //    activeNotifyIcon();
            //} 
           // this.Close();
           // Application.Exit();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            //if (Settings.Default.IsFirstLood == "1")
            //{
            //    //ShowWindow(this.Handle, SW_HIDE);
            //    this.WindowState = FormWindowState.Minimized;
            //}
            //else
            //{
            //    Settings.Default.IsFirstLood = "1";
            //    Settings.Default.Save();
            //}
           
        }

        private void Dashboard_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                activeNotifyIcon();
                // Do your action
            }
        }

       

    }
}

