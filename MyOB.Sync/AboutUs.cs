﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Globalization;

namespace MyOB.Sync
{
    public partial class AboutUs : Form
    {
        String aboutUsBody="Copyright © 2015 Suppliers Portal and/or its affiliates. All rights reserved.For more information about Suppliers Portal, visit http://www.suppliersportal.com.au";
        public AboutUs()
        {
            InitializeComponent();
            
            Version v = Assembly.GetExecutingAssembly().GetName().Version;
            lblHeadinh2.Text = string.Format(CultureInfo.InvariantCulture, @"Version {0}.{1}.{2}, build-{3}", v.Major, v.Minor, v.Revision, v.Build);
            txtAboutUs.Text = aboutUsBody;

        }
    }
}
