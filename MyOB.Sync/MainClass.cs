﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyOB.Sync.Properties;
using MyOB.Interface;
using System.Windows.Forms;
using System.Configuration;
namespace MyOB.Sync
{
    //Main class form  
    public static class MainClass
    {
        /// <summary>
        /// Testing Internet Connectivity
        /// </summary>
        /// <returns></returns>
        public static bool TcpSocketTest()
        {
            try
            {
                System.Net.Sockets.TcpClient client =
                    new System.Net.Sockets.TcpClient("www.google.com", 80);
                client.Close();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Static connection setting class and function 
    /// </summary>
    public static class ConnectionSetting{
    
        //MyOb connection setting function 
        public static MYOConnectionSetting OpenMyObConnection()
        {
            try
            {
           
            MYOConnectionSetting _objSetting = new MYOConnectionSetting();
            if (Settings.Default.MYOBFilePath != null && Settings.Default.MYOBFilePath != String.Empty)
            {
                _objSetting.MYOBFilePath = Settings.Default.MYOBFilePath;
                _objSetting.UID = Settings.Default.UID;
                _objSetting.PWD = Settings.Default.PWD;
                _objSetting.MYOBHostExePath = Settings.Default.MyoHostExePath;
                _objSetting.MYOBKey = Settings.Default.MyoKey;
                return _objSetting;
            }
            return null;
            }
            catch (Exception )
            {

                return null;
            }
        
        }

        //Opne MySQL database connection 
        public static void OpenMySqlConnection()
        {
            try
            {
                ConnectionCLS _objConnection = new ConnectionCLS();
                if (!_objConnection.MySQlConnectionOpen(mySQLConnectionSetting()))
                {
                    MessageBox.Show("MySQl database not wotking.");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error on MainClass.cs in MySQLconnection function : " + ex.Message);
            }
        }

        public static string mySQLConnectionSetting()
        {

            return "server=" + Settings.Default.mysqlServer + ";user id=" + Settings.Default.mysqlUID + ";password=" + Settings.Default.mysqlPWD + ";database=" + Settings.Default.mysqlDatabase + ";";
        }

       
    }
}
