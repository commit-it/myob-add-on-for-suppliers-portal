﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Diagnostics;
using MyOB.Sync.Properties;
using System.Windows.Forms;

using System.Configuration;
using System.Collections.Specialized;
namespace MyOB.Sync
{
    [RunInstaller(true)]
    public class InstallerClass : System.Configuration.Install.Installer
    {
        string _path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\MyOB.Sync.exe";
        public InstallerClass()
            : base()
        {
            // Attach the 'Committed' event.
            this.Committed += new InstallEventHandler(MyInstaller_Committed);
            // Attach the 'Committing' event.
            this.Committing += new InstallEventHandler(MyInstaller_Committing);
        }

        // Event handler for 'Committing' event.
        private void MyInstaller_Committing(object sender, InstallEventArgs e)
        {
            //Console.WriteLine("");
            //Console.WriteLine("Committing Event occurred.");
            //Console.WriteLine("");
        }
        // Event handler for 'Committed' event.
        private void MyInstaller_Committed(object sender, InstallEventArgs e)
        {
            try
            {
                //Directory.SetCurrentDirectory(Path.GetDirectoryName
                //(Assembly.GetExecutingAssembly().Location));
                //Process.Start(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\MyOB.Sync.exe");
            }
            catch
            {
                // Do nothing... 
            }
        }

        // Override the 'Install' method.
        public override void Install(IDictionary savedState)
        {
        
                CustomWindow _customWindow = new CustomWindow();
                _customWindow.ShowDialog();
          
                base.Install(savedState);
            

            //string GLNNumber = this.Context.Parameters["GLNNumber"].Trim();
           
            //if (GLNNumber == "")
            //{
            //    throw new InstallException("GLN Number Not Specified!");
            //}
          
            //else
            //{
            //    // Open App.Config of executable
            //    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(_path);
            //    // Add an Application Setting.
            //    config.AppSettings.Settings.Remove("GLNNumber");
            //    config.AppSettings.Settings.Add("GLNNumber", GLNNumber);
            //    // Save the configuration file.
            //    config.Save(ConfigurationSaveMode.Modified);
            //    // Force a reload of a changed section.
            //    ConfigurationManager.RefreshSection("appSettings");

            //    base.Install(savedState);
            //}
        }
        // Override the 'Commit' method.
        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
        }

        // Override the 'Rollback' method.
        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }
        public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle,
                System.Globalization.CultureInfo.CurrentCulture, out result);
        }
    }
}
