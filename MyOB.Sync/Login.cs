﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyOB.Sync.Properties;

namespace MyOB.Sync
{
    public partial class Login : Form
    {
        public bool isLogin = false;
        public Login()
        {
            InitializeComponent();
          //  this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (Settings.Default.ServerScreenPassword == txtPassword.Text.Trim())
            {
                isLogin = true;
            }
            else
            {

                MessageBox.Show("wrong password please try again"); 
                this.DialogResult = DialogResult.None;
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin.PerformClick();
                // these last two lines will stop the beep sound
                e.SuppressKeyPress = true;
                e.Handled = true;
            }
        }
    }
}
