﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyOB.Sync.Properties;
using MyOB.Interface;

namespace MyOB.Sync
{
    public partial class MySQLDataBaseSettings : Form
    {
        public MySQLDataBaseSettings()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            loadSettings();
        }
        private void loadSettings()
        {
            try
            {
                 txtServer.Text=Settings.Default.mysqlServer;
                 txtUID.Text=Settings.Default.mysqlUID ;
                 txtPWD.Text=Settings.Default.mysqlPWD ;
                 txtDatabase.Text=Settings.Default.mysqlDatabase ;
            }
            catch (Exception)
            {
                
                
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                //submit logic
                btnSave.PerformClick();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkValidation())
                {
                    return;
                }

                ConnectionCLS _objConnection = new ConnectionCLS();
                  string message;
                  string _conn = "server=" + txtServer.Text + ";user id=" + txtUID.Text + ";password=" + txtPWD.Text + ";database=" + txtDatabase.Text + ";";
                  if (_objConnection.MySQlConnectionOpen(_conn, out  message))
                  {
                      Settings.Default.mysqlServer = txtServer.Text;
                      Settings.Default.mysqlUID = txtUID.Text;
                      Settings.Default.mysqlPWD = txtPWD.Text;
                      Settings.Default.mysqlDatabase = txtDatabase.Text;
                      Settings.Default.Save();
                  }
                  MessageBox.Show(message);
            }
            catch (Exception)
            {
                
               //throw;
            }
        }
        private bool checkValidation()
        {
            bool _return = false;
            if (txtServer.Text == "")
            {
                errorProvider.SetError(txtServer, "Please enter the mysql server name.");
                _return = true;
            }

            if (txtUID.Text == "")
            {
                errorProvider.SetError(txtUID, "Please enter the mysql database User Name.");
                _return = true;
            }


            if (txtPWD.Text == "")
            {
                errorProvider.SetError(txtPWD, "Please enter the mysql database password.");
                _return = true;
            }

            if (txtDatabase.Text == "")
            {
                errorProvider.SetError(txtDatabase, "Please enter the mysql database name.");
                _return = true;
            }


            return _return;
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            txtDatabase.Text = "";
            txtPWD.Text = "";
            txtServer.Text = "";
            txtUID.Text = "";
        }
    }
}
