﻿namespace MyOB.Sync
{
    partial class AboutUs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutUs));
            this.lblHeading1 = new System.Windows.Forms.Label();
            this.lblHeadinh2 = new System.Windows.Forms.Label();
            this.txtAboutUs = new System.Windows.Forms.RichTextBox();
            this.lblFooter = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeading1
            // 
            this.lblHeading1.AutoSize = true;
            this.lblHeading1.Location = new System.Drawing.Point(9, 12);
            this.lblHeading1.Name = "lblHeading1";
            this.lblHeading1.Size = new System.Drawing.Size(139, 13);
            this.lblHeading1.TabIndex = 0;
            this.lblHeading1.Text = "Suppliers Portal MyOB Sync";
            // 
            // lblHeadinh2
            // 
            this.lblHeadinh2.AutoSize = true;
            this.lblHeadinh2.Location = new System.Drawing.Point(9, 36);
            this.lblHeadinh2.Name = "lblHeadinh2";
            this.lblHeadinh2.Size = new System.Drawing.Size(88, 13);
            this.lblHeadinh2.TabIndex = 1;
            this.lblHeadinh2.Text = "1.0, build number";
            // 
            // txtAboutUs
            // 
            this.txtAboutUs.Location = new System.Drawing.Point(12, 64);
            this.txtAboutUs.Name = "txtAboutUs";
            this.txtAboutUs.Size = new System.Drawing.Size(288, 96);
            this.txtAboutUs.TabIndex = 2;
            this.txtAboutUs.Text = "";
            // 
            // lblFooter
            // 
            this.lblFooter.AutoSize = true;
            this.lblFooter.Location = new System.Drawing.Point(12, 285);
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Size = new System.Drawing.Size(212, 13);
            this.lblFooter.TabIndex = 3;
            this.lblFooter.Text = " The Suppliers Portal MyOB Sync is running";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MyOB.Sync.Properties.Resources.Suppliers_Portal_Font_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(3, 167);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(305, 98);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // AboutUs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 311);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblFooter);
            this.Controls.Add(this.txtAboutUs);
            this.Controls.Add(this.lblHeadinh2);
            this.Controls.Add(this.lblHeading1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutUs";
            this.Text = "About Suppliers Portal MyOB Sync";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeading1;
        private System.Windows.Forms.Label lblHeadinh2;
        private System.Windows.Forms.RichTextBox txtAboutUs;
        private System.Windows.Forms.Label lblFooter;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}