﻿namespace MyOB.Sync
{
    partial class CustomWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomWindow));
            this.txtGLNNumber = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblGLNNumber = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtGLNNumber
            // 
            this.txtGLNNumber.Location = new System.Drawing.Point(37, 32);
            this.txtGLNNumber.Name = "txtGLNNumber";
            this.txtGLNNumber.Size = new System.Drawing.Size(174, 20);
            this.txtGLNNumber.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(37, 71);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblGLNNumber
            // 
            this.lblGLNNumber.AutoSize = true;
            this.lblGLNNumber.Location = new System.Drawing.Point(37, 13);
            this.lblGLNNumber.Name = "lblGLNNumber";
            this.lblGLNNumber.Size = new System.Drawing.Size(69, 13);
            this.lblGLNNumber.TabIndex = 2;
            this.lblGLNNumber.Text = "GLN Number";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(119, 70);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // CustomWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 116);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblGLNNumber);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtGLNNumber);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomWindow";
            this.Text = "GLN Number Setting";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtGLNNumber;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblGLNNumber;
        private System.Windows.Forms.Button btnClose;
    }
}