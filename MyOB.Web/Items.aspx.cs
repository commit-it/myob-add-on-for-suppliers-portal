﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyOB.BAL;

namespace MyOB.Web
{
    public partial class Items : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                //calling getItems function  
                getItems();
            }
        }
        /// <summary>
        /// Bind Items data in grid
        /// </summary>
        private void getItems()
        {
            try
            {
                //ItemsClass _objItems = new ItemsClass(Server.MapPath("Clearwtr.MYO"));

                //gridList.DataSource = _objItems.getItems();
                //gridList.DataBind();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error on Items page and funcation name is getItems: ERROR :- " + ex.Message);
            }
        }
    }
}
