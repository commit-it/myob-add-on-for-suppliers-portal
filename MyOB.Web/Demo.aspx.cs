﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyOB.BAL;
using MyOB.Interface;

namespace MyOB.Web
{
    public partial class Demo : System.Web.UI.Page
    {
        ConnectionCLS _objConnection = new ConnectionCLS();
        protected void Page_Load(object sender, EventArgs e)
        {
            MYOConnectionSetting _objSetting = new MYOConnectionSetting();
            _objSetting.MYOBFilePath = Server.MapPath("Clearwtr.MYO");
            _objSetting.UID = "Administrator";
            _objSetting.PWD = "";
           // PurchasesClass _obj = new PurchasesClass(_objSetting);
          //  GridView1.DataSource = _obj.GetPurchases();

            //GridView1.DataSource = _obj.MySQlSelect();
            GridView1.DataBind();
        }
    }
}