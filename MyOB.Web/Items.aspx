﻿<%@ Page Title="Items " Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Items.aspx.cs" Inherits="MyOB.Web.Items" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Items
    </h2>
   <br />
   <asp:GridView ID="gridList" runat="server" AutoGenerateColumns="false" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
        CellPadding="3"
   >
        <Columns>
        
            <asp:TemplateField>
                <HeaderTemplate>
                Item Number.
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("ItemNumber")%>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
               Item Name.
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("ItemName")%>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <HeaderTemplate>
              On Hand
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("QuantityOnHand")%>
                </ItemTemplate>
            </asp:TemplateField>
            
             <asp:TemplateField>
                <HeaderTemplate>
               Last Cost.
                </HeaderTemplate>
                <ItemTemplate>
                    $<%#Eval("TaxInclusiveLastPurchasePrice")%>
                </ItemTemplate>
            </asp:TemplateField>

             <asp:TemplateField>
                <HeaderTemplate>
               Sell Price.
                </HeaderTemplate>
                <ItemTemplate>
                    $<%#Eval("BaseSellingPrice")%>
                </ItemTemplate>
            </asp:TemplateField>

             <%--<asp:TemplateField>
                <HeaderTemplate>
               Item Description.
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("ItemDescription")%>
                </ItemTemplate>
            </asp:TemplateField>--%>

           
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />
   </asp:GridView>
</asp:Content>
