﻿<%@ Page Title="Customers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Customers.aspx.cs" Inherits="MyOB.Web.Customers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <h2>
        Customers
    </h2>
   <br />
   <asp:GridView ID="gridList" runat="server" AutoGenerateColumns="false" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
        CellPadding="3"
   >
        <Columns>
        
            <asp:TemplateField>
                <HeaderTemplate>
               Name.
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("FirstName")%> <%#Eval("LastName")%>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField>
                <HeaderTemplate>
               Card Identification.
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("CardIdentification")%> <%#Eval("LastName")%>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                <HeaderTemplate>
              Credit Limit.
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("CreditLimit")%>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <HeaderTemplate>
               Current Balance.
                </HeaderTemplate>
                <ItemTemplate>
                    $<%#Eval("CurrentBalance")%>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField>
                <HeaderTemplate>
               Last Sale Date
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("LastSaleDate", "{0:dd-MMM-yyyy}")%>
                </ItemTemplate>
            </asp:TemplateField>
           
        </Columns>
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />
   </asp:GridView>
</asp:Content>
