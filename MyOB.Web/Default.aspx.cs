﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyOB.BAL;
using MyOB.Interface;
using System.Data.Odbc;
using System.Data;
using System.Globalization;
namespace MyOB.Web
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //calling getSales function  
                getSales();
            }
        }
        /// <summary>
        /// Bind sales data in grid
        /// </summary>
        private void getSales()
        {
            try
            {
                ConnectionCLS _objConnection = new ConnectionCLS();
                MYOConnectionSetting _objSetting = new MYOConnectionSetting();
                _objSetting.MYOBFilePath = @"E:\myobfile\Clearwtr.MYO";
                _objSetting.UID = "Administrator";
              //  _objSetting.PWD = "Welcome01";
                _objSetting.MYOBHostExePath = @"C:\Premier19\Myobp.exe";
                _objSetting.MYOBKey = "198987B68BFEE6C2E2D7B0846CBCD1F67710B41AE0882CFAAF18C4B6BDF7C2A9AC02F2FEA21C8A8E4C1ACF7989F716A67FC0C5D7420744A77EFDF61D0569AA0D";
                _objConnection.ConnectionOpen(_objSetting); 
                DataSet dsInvoiceItems = new DataSet();
                string _date =setDateformat("2015-08-25");
                string _query = string.Empty;
                _query = "Select * from Sales ";
                //_query = "Select COUNT(*)as Total from customers c where (c.LastName='ok')";
                using (OdbcDataAdapter dataAdapter = new OdbcDataAdapter(_query, _objConnection.odbc))
                {
                   
                    dataAdapter.Fill(dsInvoiceItems);
                    _objConnection.ConnectionClose();
                    //string _mdate = setDateformat(dsInvoiceItems.Tables[0].Rows[0]["Date"].ToString());
                    //if (DateTime.Parse(_mdate) <= DateTime.Parse(_date))
                    //{
                    //    Response.Write("true");
                    //    // perform some code here
                    //}
                //    return dsInvoiceItems.Tables[0];
                }

                gridList.DataSource = dsInvoiceItems;
                gridList.DataBind();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error on Sale page and funcation name is getSales: ERROR :- " + ex.Message);
            }
        }
        private  string setDateformat(string date)
        {
            try
            {  //
                string sysFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
                if (sysFormat == "")
                {
                    sysFormat = "MM-dd-yy";
                }
                return Convert.ToDateTime(date).ToString(sysFormat);
            }
            catch (Exception ex)
            {
                //ErrorClass.WriteError("Error in Common class, function name setDateformat[MyOB.DAL] :-" + ex.Message);
                throw;
            }
        }
    }
}
