﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyOB.BAL;
using MyOB.Interface;

namespace MyOB.Web
{
    public partial class Customers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //calling getCustomers function  
                getCustomers();
            }
        }
        /// <summary>
        /// Bind Customers data in grid
        /// </summary>
        private void getCustomers()
        {
            try
            {
                MYOConnectionSetting _objSetting = new MYOConnectionSetting();
                _objSetting.MYOBFilePath = Server.MapPath("Clearwtr.MYO");
                _objSetting.UID = "Administrator";
                _objSetting.PWD = "";
                CustomersClass _objItems = new CustomersClass(_objSetting,null);

                gridList.DataSource = _objItems.getCustomers();
                gridList.DataBind();
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error on Items page and funcation name is getCustomers: ERROR :- " + ex.Message);
            }
        }
    }
}